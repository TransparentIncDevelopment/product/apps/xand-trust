#!/bin/bash

# Prints the git diff of uncommitted (working-tree) changes in each crate's Cargo.toml.
# Used to record the changes made by CI when it is applying edits for publishing.

set -e

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

MASTER_TOML="$DIR/../trustee-node/Cargo.toml"
CONFIG_TOML="$DIR/../trustee-node-config/Cargo.toml"

git diff "$MASTER_TOML" "$CONFIG_TOML"
