#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail # abort on pipeline error

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to verify that the given git tag matches the versions of the two crates, trustee-node
    and trustee-node-config.

    Arguments
        TAG (Required) = The name of the tag committed to the CI
END
)
echo "$HELPTEXT"
}

function error {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

if [[ $# -eq 0 ]] ; then
    error 'No arguments provided'
fi

TAG=${1:?"$(error 'TAG must be set' )"}

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

MASTER_TOML="$DIR/../trustee-node/Cargo.toml"
CONFIG_TOML="$DIR/../trustee-node-config/Cargo.toml"

MASTER_VERSION=$(toml get "$MASTER_TOML" package.version | tr -d \")
CONFIG_VERSION=$(toml get "$CONFIG_TOML" package.version | tr -d \")

if [ "v${MASTER_VERSION}" == $TAG ] ; then
    echo "Tag matches trustee-node version"
else
    echo "Tag does NOT match trustee-node version. Tag: ${TAG}, Version: ${MASTER_VERSION}"
    exit 1
fi

if [ "v${CONFIG_VERSION}" == $TAG ] ; then
    echo "Tag matches trustee-node-config version"
else
    echo "Tag does NOT match trustee-node-config version. Tag: ${TAG}, Version: ${CONFIG_VERSION}"
    exit 1
fi

