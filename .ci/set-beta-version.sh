#!/bin/bash

set -e

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

MASTER_TOML="$DIR/../trustee-node/Cargo.toml"

CURRENT_VERSION=$(toml get "$MASTER_TOML" package.version | tr -d '"')
BETA_VERSION="$CURRENT_VERSION-beta.${CI_PIPELINE_IID}"

echo "Setting beta version in "$MASTER_TOML" -> $BETA_VERSION"
NEW_CARGO_TOML=$(toml set "$MASTER_TOML" package.version $BETA_VERSION)
echo "$NEW_CARGO_TOML" > "$MASTER_TOML"
