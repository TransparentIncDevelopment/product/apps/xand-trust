#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by humans.
    The purpose of this script is to be able to automate the tagging of a release to
    standardize tags and pushing.

    There should not be any untracked changes in your repository before incrementing.
    Also this is intended to run when on the master branch.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

options=$(getopt -o h --long help -- "$@")

if [[ $# -ne 0 ]] ; then
    while true; do
        case "$1" in
            -h|--help)
                echo "$(helptext)"
                exit 1
                ;;
            --)
                shift
                break
                ;;
            *);;
        esac
        shift
    done
fi

git diff-index --quiet HEAD && DIFF=$? || DIFF=$?

if [[ $DIFF -ne 0 ]] ; then
    error "ERROR: You have tracked changes that are uncommitted. You should commit or clear before publishing a version."
    exit 1
fi

source ${BASH_SOURCE%/*}/util.sh

CURRENT_VERSION=$(get_version trustee-node/Cargo.toml)
echo "Creating tag for current version: $CURRENT_VERSION"

git tag v$CURRENT_VERSION
echo "Tag v$CURRENT_VERSION created."
echo "Run git push --tags"
