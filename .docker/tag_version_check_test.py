from tag_version_check import run
import pytest


# How to run these tests:
# 1. run `make test` from this folder

def create_test_tags_file():
    f = open("test-tags.txt", "w")
    f.write("2.2.2\n")
    f.write("v2.2.3\n")
    f.write("v2.2.4\n")
    f.close()

create_test_tags_file()

def test_lesser_versions_fail_check():
    assert run("v1.1.0", "test-tags.txt") == False
    assert run("1.1.1", "test-tags.txt") == False
    assert run("1.10.1", "test-tags.txt") == False
    assert run("v1.1.10", "test-tags.txt") == False
    assert run("2.2.1", "test-tags.txt") == False
    assert run("2.2.2", "test-tags.txt") == False

def test_greater_versions_pass_check():
    assert run("2.2.5", "test-tags.txt")
    assert run("v2.2.10", "test-tags.txt")
    assert run("v2.10.1", "test-tags.txt")
    assert run("3.1.1", "test-tags.txt")
    assert run("3.3.1", "test-tags.txt")

    assert run("v10.1.5", "test-tags.txt")
    assert run("10.2.5", "test-tags.txt")
    assert run("10.10.10", "test-tags.txt")
    assert run("v100.100.100", "test-tags.txt")

def test_pre_release_tags_always_pass_check():
    assert run("1.1.1-alpha-123", "test-tags.txt")
    assert run("v1.20.30-beta123", "test-tags.txt")
    assert run("2.2.0-beta.123", "test-tags.txt")
    assert run("v2.3.4-beta123", "test-tags.txt")
    assert run("v3.1.1-beta123", "test-tags.txt")





