# Creating a trust docker container with the current code state

The purpose of this directory is to provide a way for the trust integration tests to build a trust docker image that 
aligns with the local state of the code.

To achieve this, a docker image (see xand-trust/.docker/integ-test/Dockerfile.compiler) is built and run to compile a
release binary for a trustee-node and then a docker image (see xand-trust/.docker/Dockerfile) is built to run the binary
in integration testing. The binary is built from within the docker container (that has the same base image in which it 
will be run) to prevent dynamic linking issues.

## Workflow

The Makefile in this repo does the following and is intended to be used in this order:

### run-compiler

`make run-compiler` first runs the `build-compiler` target. Runs the docker image built in the `build-compiler` target. 
Assumes the local environment has a valid ssh key to access the TPFS Gitlab and that key lives in a standard ssh key 
file path as specified under [Adding Default Keys](https://www.ssh.com/academy/ssh/add).

### build

`make build` first runs the `make artifacts` target. Builds the docker image that contains the trustee-node binary
produced by the trust-compiler docker container.

## Other Makefile Targets

### build-compiler

`make build-compiler` builds the docker image to compile the trustee-node binary.

### artifacts

`make artifacts` copies the trustee-node binary into `.docker/integ-test/tmp` so that it is accessible by the Dockerfile.
Assumes that the trust software has been compiled for the release target to `docker-targe` by the trust-compiler docker 
container.
