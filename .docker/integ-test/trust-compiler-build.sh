#!/bin/bash

# This script is intended to be run from within the trust-compiler docker container. It authenticates via ssh and then
# builds a release version of the trustee-node from within the container.

eval $(ssh-agent)

if [[ -v CI_SSH_KEY ]]; then
  ssh-add - <<< "${CI_SSH_KEY}"
else
  ssh-add
fi

# Add gitlab.com's ssh key to known_hosts so that cargo build can fetch from tpfs repo
mkdir -p /root/.ssh
ssh-keyscan gitlab.com > /root/.ssh/known_hosts

source $HOME/.cargo/env
cd xand-trust
cargo build --release --all-features --target-dir ./docker-target


