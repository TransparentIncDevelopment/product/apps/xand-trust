# Getting Started

The XAND trust administration software is software run by the trust administrator that supports the automated functions by which members can move money between their bank accounts at participating banks and the XAND distributed ledger.

## Usage

This repo uses the standard cargo build and test verbs defined at https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide/-/blob/master/developer-experience.md

## Dependencies
The function of this software depends on access to a XAND API for ledger access as well as bank APIs.

## Dev Setup

The trust software connects to a running network.

### Generating the network

A generated network lives in `./xng/generated` and should not be modified directly.

It is generated using [`xand_network_generator`](https://gitlab.com/TransparentIncDevelopment/product/testing/xand_network_generator). 

(Re-)generate the network with:

```bash
make install-xng
make download-chain-spec
make generate
``` 

Metadata and URLs to access the docker-compose services from the host machine can be found in the `./xng/generated/entities-metadata.yaml` file.

Follow these steps to set up the `./integ-test-config/` directory to enable the Trust instance
running for the integration tests (outside the docker-compose network) to work with the services
within the docker-compose context. 

1. Copy the generated config to the integ-test-config folder
```bash
cp -r ./xng/generated/trust/trust-software/config/* integ-test-config/
```

1. Update trust config value in `integ-test-config/` to: `transaction_db_path: "/etc/xand/trustee-node-db/integ_test.sqlite3"`

2. Update bank urls in `integ-test-config/` to be `localhost` instead of `bank-mocks`, since the 
Trust instance of the integration tests is outside docker-compose.

3. Simliarly, update the `xand_api_endpoint` to the trust's non-consensus node (eg trust node) URL, found in the `xng/generated/entities-metdata.yaml` file
   
4. Update the config for any breaking changes local to your branch in this repo.

### Integration Tests

You can run a local network and corresponding integration tests using `make run`. 
After the integration tests finish the network continues running. 
Subsequent executions automatically ensure the network and state are properly cleaned up first.

Internally, the make task leverages the docker-compose network defined in `./xng/generated`. 

Start the network and run integration tests with 
```bash
make run
# For more verbose output (including logs from the trust node)
make run-verbose
```

The Trustee component of XNG is not used for integ tests. When XNG starts, the `Makefile` shuts down the Trustee 
included with XNG. Instead, this local repo is compiled and a local docker image `gcr.io/xand-dev/trust:latest` is 
built. The integ tests start a docker container in the XNG network using this docker image. This ensures integ tests
run against the latest Trust software under development, and not an old published image. 

#### OPENSSL_1_1_1 error
You may encounter an OPENSSL error when trying to run integ_tests `make run`. This is a known issue; See here
for suggested workarounds: https://github.com/docker/compose/issues/5930
 

### Snapshot Tests

This repo uses the `insta` crate for snapshot testing. 

Install their CLI with:
```bash
cargo install cargo-insta
```

When you see a failing snapshot test, run 
```bash
cargo insta review
```

`cargo insta review` can help you both assess whether the new snapshot looks correct (e.g. by 
diffing with a pre-existing one) and approve it as the new authoritative snapshot

See https://insta.rs for a good intro to snapshot tests. In particular, see 
their ["Test Workflow"](https://insta.rs/#test-workflow).
