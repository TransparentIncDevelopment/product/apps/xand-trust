# Contributing

## Dependencies

Cargo toml CLI

```
cargo install cargo-toml
```

Cargo audit

```
cargo install cargo-audit
```

## Running Linting and Unit Tests

```shell
make test
```

## Running Network Integration Testing

```shell
make integ-test
```

## Running Auditing

```shell
cargo audit
```

## Publishing of Prerelease Crates

Please see the versioning guidelines to understand prereleases: https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide/blob/versioning-proposal/versioning.md

You should use prereleases to test out a concept in a different docker image. However this is not intended for your final releases. See [Publishing changes to a crate](#publishing-a-version-change-to-release) for finalizing.

You can manually push a prerelease version for a branch by finding the corresponding pipeline in gitlab and clicking the play button next to the `manual-publish-prerelease-crate` job.
Prerelease crates will also be automatically published as merges occur to master.

## Publishing a version change to release

The versioning of the crate should be maintained via [semantic versioning](https://semver.org/) and there are helper scripts to help update the version in the Cargo.toml and Cargo.lock files. This version change can happen on your branch hopefully right before the MR is approved to be merged.

Use one of the following:
* `make publish-patch`
* `make publish-minor`
* `make publish-major`

In order to actually publish the change this should be done after an MR has merged to master. Once merged pull from master and rebase then tag a release using the script helper.

It would look like the following:

```shell
git checkout master
git pull
make tag-release
git push --tags
```

## Understanding Semantic Versioning
We have something written up in our engineering guidelines: https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide/blob/versioning-proposal/versioning.md

The basics of semver is if the only changes included are fixes and adjustments then it's just a patch version number bump. Then if there are additions to functionality it's a minor version bump, and if it's a breaking change then it's a major number bump. The precedence is set at if there's a major version bump for a breaking change it would encompass any additional functionality without needed to bump the minor version. The same is true for minor version bumps that take priority over a patch version bump.

An example of that would be if the version number is `1.0.0` and there was additional functionality along with some fixes this would update the version to `1.1.0`.
