//! This module recreates the `xand-banks` config structs as they were in 0.1 in order to prevent
//! needing to make radical changes to the way trust configuration is currently done.
//!
//! We can and should feel free to adjust these config structs however we feel is appropriate for
//! the trusts' needs. They will then be converted to the `xand-banks` equivalents for usage.
use bank_transaction_repository::BankClientConfiguration;
use url::Url;
use xand_banks::banks::{
    adapter_config::AdapterConfig,
    config::BankConfig,
    mcb_adapter::config::{
        McbClientTokenRetrievalAuthConfig, McbConfig as McbAdapterConfig,
        RawAccountInfoManagerConfig,
    },
    treasury_prime_adapter::config::{
        TreasuryPrimeAuthConfig, TreasuryPrimeConfig as TreasuryPrimeAdapterConfig,
    },
};
use xand_banks::constants::BANK_CALL_STANDARD_TIMEOUT;

#[serde_with::skip_serializing_none]
#[derive(Clone, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
#[serde(deny_unknown_fields, rename_all = "snake_case")]
pub enum TrustBanksConfig {
    Mcb(McbConfig),
    TreasuryPrime(TreasuryPrimeConfig),
}

/// Required configuration values to start using the MCB adapter
#[derive(Clone, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub struct McbConfig {
    /// The host url to hit. For example, MCB's test env host url is https://dev.mcb-api.com
    #[serde(with = "url_serde")]
    pub bank_api_url: Url,
    pub routing_number: String,
    pub trust_account: String,
    pub refresh_if_expiring_in_secs: Option<u64>,
    pub secret_key_username: String,
    pub secret_key_password: String,
    pub secret_key_client_app_ident: String,
    pub secret_key_organization_id: String,
    pub display_name: String,
}

impl From<McbConfig> for BankConfig {
    fn from(c: McbConfig) -> Self {
        BankConfig {
            name: c.display_name,
            routing_number: c.routing_number,
            trust_account: c.trust_account,
            accounts: vec![],
            adapter: AdapterConfig::Mcb(McbAdapterConfig {
                url: c.bank_api_url,
                refresh_if_expiring_in_secs: None,
                auth: McbClientTokenRetrievalAuthConfig {
                    secret_key_username: c.secret_key_username,
                    secret_key_password: c.secret_key_password,
                },
                account_info_manager: RawAccountInfoManagerConfig {
                    secret_key_client_app_ident: c.secret_key_client_app_ident,
                    secret_key_organization_id: c.secret_key_organization_id,
                },
                timeout: BANK_CALL_STANDARD_TIMEOUT,
            }),
        }
    }
}

impl From<McbConfig> for BankClientConfiguration {
    fn from(cfg: McbConfig) -> Self {
        Self::Mcb(McbAdapterConfig {
            url: cfg.bank_api_url,
            refresh_if_expiring_in_secs: cfg.refresh_if_expiring_in_secs,
            auth: McbClientTokenRetrievalAuthConfig {
                secret_key_username: cfg.secret_key_username,
                secret_key_password: cfg.secret_key_password,
            },
            account_info_manager: RawAccountInfoManagerConfig {
                secret_key_client_app_ident: cfg.secret_key_client_app_ident,
                secret_key_organization_id: cfg.secret_key_organization_id,
            },
            timeout: BANK_CALL_STANDARD_TIMEOUT,
        })
    }
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub struct TreasuryPrimeConfig {
    #[serde(with = "url_serde")]
    pub bank_api_url: Url,
    pub routing_number: String,
    pub trust_account: String,
    pub secret_key_username: String,
    pub secret_key_password: String,
    pub display_name: String,
}

impl From<TreasuryPrimeConfig> for BankConfig {
    fn from(c: TreasuryPrimeConfig) -> Self {
        BankConfig {
            name: c.display_name,
            routing_number: c.routing_number,
            trust_account: c.trust_account,
            accounts: vec![],
            adapter: AdapterConfig::TreasuryPrime(TreasuryPrimeAdapterConfig {
                url: c.bank_api_url,
                auth: TreasuryPrimeAuthConfig {
                    secret_key_username: c.secret_key_username,
                    secret_key_password: c.secret_key_password,
                },
                timeout: BANK_CALL_STANDARD_TIMEOUT,
            }),
        }
    }
}

impl From<TreasuryPrimeConfig> for BankClientConfiguration {
    fn from(cfg: TreasuryPrimeConfig) -> Self {
        Self::Provident(TreasuryPrimeAdapterConfig {
            url: cfg.bank_api_url,
            auth: TreasuryPrimeAuthConfig {
                secret_key_username: cfg.secret_key_username,
                secret_key_password: cfg.secret_key_password,
            },
            timeout: BANK_CALL_STANDARD_TIMEOUT,
        })
    }
}
