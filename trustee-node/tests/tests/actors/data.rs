use xand_api_client::{Blockstamp, TransactionId};
use xand_network_generator::contracts::data::bank_info::Bank;

//Underlying amount is minor units (e.g. 5000 = $50)
pub type Amount = u128;

#[derive(Debug)]
pub struct XandTxnReceipt {
    pub txn_id: TransactionId,
}

pub struct Block(pub(super) Blockstamp);

impl Block {
    pub fn num(&self) -> u64 {
        self.0.block_number
    }
}

pub struct BankAccount {
    _bank: Bank,
    account_num: String,
}

impl BankAccount {
    pub fn new(bank: Bank, account_num: &str) -> Self {
        Self {
            _bank: bank,
            account_num: account_num.to_string(),
        }
    }

    pub fn account_number(&self) -> String {
        self.account_num.to_string()
    }
}
