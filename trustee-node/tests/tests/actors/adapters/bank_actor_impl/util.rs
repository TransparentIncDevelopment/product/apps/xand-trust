use xand_secrets::{CheckHealthError, ReadSecretError, Secret, SecretKeyValueStore};

pub struct FakeSecrets;
#[async_trait::async_trait]
impl SecretKeyValueStore for FakeSecrets {
    async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError> {
        Ok(Secret::new(key.to_string()))
    }
    async fn check_health(&self) -> Result<(), CheckHealthError> {
        Ok(())
    }
}
