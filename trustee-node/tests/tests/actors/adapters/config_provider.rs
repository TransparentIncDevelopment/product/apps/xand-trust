use crate::tests::actors::ports::config_provider::ConfigProvider;
use xand_network_generator::contracts::{
    data::bank_info::Bank,
    network::metadata::xand_entity_set_summary::{MemberMetadata, XandEntitySetSummary},
};

const ENTITIES_METADATA_FILEPATH: &str = "../xng/generated/entities-metadata.yaml";

/// This struct is an adapter around the `XandEntitySetSummary` type,
/// enabling it to be a ConfigProvider
pub struct XandEntitySetSummaryConfigProvider {
    pub metadata: XandEntitySetSummary,
}

impl XandEntitySetSummaryConfigProvider {
    pub fn new() -> Self {
        let metadata = XandEntitySetSummary::load(ENTITIES_METADATA_FILEPATH)
            .expect("Could not load entities");
        Self { metadata }
    }
}

impl ConfigProvider for XandEntitySetSummaryConfigProvider {
    fn mcb_url(&self) -> String {
        self.metadata
            .banks
            .urls
            .get(&Bank::Mcb)
            .expect("Must have MCB url in config file")
            .clone()
    }

    fn member_metadata_at_index(&self, index: usize) -> MemberMetadata {
        self.metadata
            .members
            .get(index)
            .expect("Member must exist at requested index")
            .clone()
    }

    fn trust_mcb_account(&self) -> String {
        self.metadata
            .trust
            .banks
            .iter()
            .find_map(|b| {
                if b.bank == Bank::Mcb {
                    Some(b.account.account_number().to_string())
                } else {
                    None
                }
            })
            .expect("Trust must have an MCB bank account")
    }
}
