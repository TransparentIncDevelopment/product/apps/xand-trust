use crate::tests::actors::{
    adapters::{
        bank_actor_impl::McbActor, config_provider::XandEntitySetSummaryConfigProvider, txn_util,
        txn_util::Fulfilled,
    },
    data::{Amount, BankAccount, Block, XandTxnReceipt},
    ports::{bank_actor::BankAccountActor, config_provider::ConfigProvider, MemberActor},
};
use core::time::Duration;
use std::{str::FromStr, thread::sleep};
use thiserror::Error;
use trustee_node::{
    async_adapter::{adapters::tokio_adapter::TokioAdapter, ports::sync_future_ext::SyncFutureExt},
    core::trim_0x_prefix,
};
use url::Url;
use xand_address::Address;
use xand_api_client::{
    errors::XandApiClientError, BankAccountId, BankAccountInfo, CorrelationId,
    PendingCreateRequest, PendingRedeemRequest, Transaction, XandApiClient, XandApiClientTrait,
    XandTransaction,
};
use xand_network_generator::contracts::{
    data::bank_info::Bank, network::metadata::xand_entity_set_summary::MemberMetadata,
};

pub struct MemberActorImpl {
    member_metadata: MemberMetadata,
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error, Clone)]
pub enum Error {
    #[error("The transaction was not fulfilled within the timeout period")]
    TransactionFulfillmentTimeOutExpired,
}

impl MemberActorImpl {
    pub fn new(m: Member) -> Self {
        match m {
            Member::Default => Self::from_entities_metadata(0),
            Member::Creator => Self::from_entities_metadata(0),
            Member::Redeemer => Self::from_entities_metadata(1),
        }
    }

    pub fn get_txn_details(
        &self,
        receipt: &XandTxnReceipt,
    ) -> Result<Transaction, XandApiClientError> {
        let xand_api_client = self.get_xand_client();
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");
        xand_api_client
            .get_transaction_details(&receipt.txn_id)
            .block_with(&tokio_handle)
    }

    pub fn wait_until_fulfilled_by_trust(&self, receipt: &XandTxnReceipt) -> Result<Fulfilled> {
        let sleep_secs = 2;
        const MAX_ATTEMPTS: usize = 30;
        for attempt in 0..MAX_ATTEMPTS {
            tracing::info!(
                "Attempt #{}/{} at ascertaining completion of transaction {}",
                attempt + 1,
                MAX_ATTEMPTS,
                receipt.txn_id,
            );
            let details = self.get_txn_details(receipt);

            let maybe_txn = txn_util::is_fulfilled(receipt, details);
            match maybe_txn {
                None => { /*do nothing; continue checking*/ }
                Some(txn) => return Ok(txn),
            }
            sleep(Duration::from_secs(sleep_secs));
        }
        Err(Error::TransactionFulfillmentTimeOutExpired)
    }

    fn from_entities_metadata(idx: usize) -> Self {
        let cfg = XandEntitySetSummaryConfigProvider::new();

        // Get member info for indexed member
        let metadata = cfg.member_metadata_at_index(idx);

        MemberActorImpl {
            member_metadata: metadata,
        }
    }

    fn get_bank_actor(&self) -> McbActor {
        // Get this member's 0th bank account; Trust integ tests assume member only has 1 MCB bank
        let bank_account_num = self
            .member_metadata
            .banks
            .get(0)
            .expect("member must have at least 1 bank");
        McbActor::new(BankAccount::new(
            bank_account_num.bank,
            &bank_account_num.account.account_number().to_string(),
        ))
    }
    pub(crate) fn get_xand_client(&self) -> XandApiClient {
        // Init client against this member's XandApi
        let xand_api_url = &self.member_metadata.xand_api_details.xand_api_url;
        // url crate from xng is 2.x, while trust uses 1.x. Serialize and deserialize to use version of Url XandApiClient/Trust use
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");
        let xand_api_url = Url::parse(xand_api_url.as_ref()).expect("Must be able to parse url");
        XandApiClient::connect(&xand_api_url)
            .block_with(&tokio_handle)
            .expect("Must be able to create xand api client")
    }
}

impl MemberActor for MemberActorImpl {
    fn full_create_claims_flow(&self, amount: Amount) -> XandTxnReceipt {
        let id = CorrelationId::gen_random();
        let receipt = self.submit_create_request(&id, amount);
        self.transfer_funds_for_create(&id, amount);
        receipt
    }

    fn full_redeem_claims_flow(&self, amount: Amount) -> XandTxnReceipt {
        let xand_api_client = self.get_xand_client();
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");

        // Get this member's 0th bank account; Trust integ tests assume member only has 1 bank
        let bank_account_num = self
            .member_metadata
            .banks
            .get(0)
            .expect("member must have at least 1 bank");

        let correlation_id = CorrelationId::gen_random();

        let bank_account_info: BankAccountInfo = BankAccountInfo::Unencrypted(BankAccountId {
            routing_number: bank_account_num.account.routing_number().to_string(),
            account_number: bank_account_num.account.account_number().to_string(),
        });

        let txn = PendingRedeemRequest {
            account: bank_account_info,
            correlation_id,
            amount_in_minor_unit: amount as u64,
            completing_transaction: None,
        };

        let address = &self.member_metadata.address;
        tracing::info!("Submitting redemption request {:?}", txn);
        let receipt = xand_api_client
            .submit_transaction_wait(
                Address::from_str(address).unwrap(),
                XandTransaction::RedeemRequest(txn),
            )
            .block_with(&tokio_handle)
            .expect("Must be able to submit Creation Request");

        XandTxnReceipt { txn_id: receipt.id }
    }

    fn get_finalized_block_num(&self) -> Block {
        let xand_api_client = self.get_xand_client();
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");

        let blockstamp = xand_api_client
            .get_current_block()
            .block_with(&tokio_handle)
            .expect("Must be able to fetch current block");

        Block(blockstamp)
    }

    fn get_xand_balance(&self) -> Amount {
        let xand_api_client = self.get_xand_client();
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");
        // Query for balance.
        let address = &self.member_metadata.address;

        let bal = xand_api_client
            .get_balance(address)
            .block_with(&tokio_handle)
            .expect("Could not fetch xand balance");
        bal.unwrap()
    }

    fn get_bank_balance(&self) -> Amount {
        let bank_actor = self.get_bank_actor();
        bank_actor.get_balance()
    }

    fn transfer_funds_for_create(&self, id: &CorrelationId, amount: Amount) {
        let cfg = XandEntitySetSummaryConfigProvider::new();

        //Do bank transfer
        let bank_actor = self.get_bank_actor();

        let id = trim_0x_prefix(&id.to_string());
        bank_actor.transfer_to(
            amount,
            id,
            BankAccount::new(Bank::Mcb, &cfg.trust_mcb_account()),
        );
    }

    fn submit_create_request(&self, id: &CorrelationId, amount: Amount) -> XandTxnReceipt {
        let xand_api_client = self.get_xand_client();
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");

        // Get this member's 0th bank account; Trust integ tests assume member only has 1 bank
        let bank_account_num = self
            .member_metadata
            .banks
            .get(0)
            .expect("member must have at least 1 bank");

        //Submit creation request
        let bank_account_info: BankAccountInfo = BankAccountInfo::Unencrypted(BankAccountId {
            routing_number: bank_account_num.account.routing_number().to_string(),
            account_number: bank_account_num.account.account_number().to_string(),
        });

        let txn = PendingCreateRequest {
            account: bank_account_info,
            correlation_id: id.clone(),
            amount_in_minor_unit: amount as u64,
            completing_transaction: None,
        };

        tracing::info!("Submitting creation request {:?}", txn);
        let address = &self.member_metadata.address;
        let receipt = xand_api_client
            .submit_transaction_wait(
                Address::from_str(address).unwrap(),
                XandTransaction::CreateRequest(txn),
            )
            .block_with(&tokio_handle)
            .unwrap_or_else(|_| panic!("Must be able to submit Creation Request: {:?}", address));

        // Return xand txn receipt
        XandTxnReceipt { txn_id: receipt.id }
    }
}

/// The different named Members the trust integ tests use
pub enum Member {
    Default,
    Creator,
    Redeemer,
}
