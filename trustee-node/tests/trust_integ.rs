#![forbid(unsafe_code)]

//! Tests that verify the trust properly observes the chain and fulfills creates/redeems
#[macro_use]
extern crate galvanic_assert;

mod docker;

#[cfg(test)]
mod tests {
    mod actors;
    mod logging;
    mod network_controller;
    use super::docker::{HoursOffset, TrusteeContainer};

    use crate::tests::{
        actors::{
            adapters::{
                config_provider::XandEntitySetSummaryConfigProvider,
                member_actor_impl::{Error as MemberActorError, Member, MemberActorImpl},
            },
            data::Amount,
            ports::MemberActor,
        },
        network_controller::{compose_env_file_location, compose_file_location},
    };
    use assert_matches::assert_matches;
    use network_maestro::{ComposeShellMaestro, ContainerInstanceId, NetworkMaestro};
    use std::time::Duration;
    use trustee_node::async_adapter::{
        adapters::tokio_adapter::TokioAdapter, ports::sync_future_ext::SyncFutureExt,
    };
    use url::Url;
    use xand_api_client::{CorrelationId, XandApiClient};
    use xand_utils::timeout;

    /// Default duration to sleep in tests, when waiting background activity to happen.
    const DEFAULT_SLEEP_SECS: Duration = Duration::from_secs(15);

    /// When testing if some queried value has changed, tests will query and check in a loop.
    /// If the value has not changed in the configured duration, the test will fail
    const TEST_TIMEOUT_SECS: Duration = Duration::from_secs(30);

    /// When testing if some queried value has changed, tests will query and check in a loop.
    /// This is the default value for waiting in between each iteration of the loop.
    const ASSERTION_LOOP_INTERVAL: Duration = Duration::from_millis(10);

    /// Steps:
    /// 1. takes down network
    /// 2. asserts that a new client cannot connect to the trust's non-consensus node
    /// 3. brings back network
    /// 4. member goes through creation flow
    /// 5. asserts that the trust fulfills it (implying it has reconnected to the node it was assumed to have lost connection to)
    #[test]
    fn trust_node_reconnects_if_validators_go_down() {
        logging::init_stdout_logging();
        let tokio_handle = TokioAdapter::get().expect("Must be able to create tokio adapter");

        // Given
        let _trustee_container = TrusteeContainer::new();
        let member_0 = MemberActorImpl::new(Member::Default);
        let create_amount = 2500;

        tracing::info!("Wait to see the network finalize at least 1 block");
        timeout!(TEST_TIMEOUT_SECS, ASSERTION_LOOP_INTERVAL, {
            let block = member_0.get_finalized_block_num();
            block.num() > 0
        });

        // When
        tracing::info!("Stopping trust non-consensus node");
        let csm =
            ComposeShellMaestro::new(compose_file_location(), compose_env_file_location(), true);

        csm.stop_trust_node()
            .block_with(&tokio_handle)
            .expect("Must stop trust non-consensus node");

        tracing::info!(
            "Asserting we cannot connect to trust node. We assume this means it is down"
        );

        let cfg = XandEntitySetSummaryConfigProvider::new();
        // Different URL versions, so converting to string first
        let url_string = cfg.metadata.trust.xand_api_details.xand_api_url.to_string();
        let trust_validator_url =
            Url::parse(&url_string).expect("Must be able to parse Xand Api URL");
        let trust_node_client_result =
            XandApiClient::connect(&trust_validator_url).block_with(&tokio_handle);
        assert!(trust_node_client_result.is_err());

        // Then
        tracing::info!("Starting trust node again");

        csm.start_trust_node()
            .block_with(&tokio_handle)
            .expect("Must start trust non-consensus node");

        tracing::info!("Sleeping {:?} white network restarts", &DEFAULT_SLEEP_SECS);
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Attempting to Create Xand claims");
        let create_receipt = member_0.full_create_claims_flow(create_amount);

        tracing::info!("Wait until CreateRequest is fulfilled");
        member_0
            .wait_until_fulfilled_by_trust(&create_receipt)
            .unwrap();
    }

    /// Steps:
    /// 1. Member 0 Creates and Transfers funds to Member 1 (Confirms trust is operating in background)
    /// 2. Take down 1 validator
    /// 3. Member 0 attempts another Create; Member 1 attempts a Redeem
    /// 4. Asserts that neither balances have changed, implying one or both:
    ///     4a. Member 1's funds are not yet locked because state incorporating its Redeem request is not finalized
    ///     4b. The Trust isn't fulfulling requests because finalized state
    /// 5. Brings back validator node
    /// 6. Asserts that balances are updated (because state starts getting finalized again, and trust starts acting on updated state)
    #[test]
    fn polling_waits_for_finalized_transactions() {
        let tokio_handle = TokioAdapter::get().expect("Could not build tokio handle");
        logging::init_stdout_logging();

        // Given
        let _trustee_container = TrusteeContainer::new();

        let setup_create_amount: Amount = 2500;
        let create_amount: Amount = 200;
        let redemption_amount: Amount = 500;

        let csm =
            ComposeShellMaestro::new(compose_file_location(), compose_env_file_location(), true);

        let redeemer = MemberActorImpl::new(Member::Redeemer);
        let creator = MemberActorImpl::new(Member::Creator);

        let creator_initial_xand_bal = creator.get_xand_balance();
        let redeemer_initial_bank_bal = redeemer.get_bank_balance();

        tracing::info!("Redeemer needs to create claims for it to redeem later");
        let setup_create_receipt = redeemer.full_create_claims_flow(setup_create_amount);
        redeemer
            .wait_until_fulfilled_by_trust(&setup_create_receipt)
            .unwrap();

        // When
        // Assumes there are less than 4 validators and killing 1 prevents finalization
        // Assumes we are not connected to "validator 1"
        tracing::info!("Killing validator 1");
        csm.stop_node(ContainerInstanceId(1))
            .block_with(&tokio_handle)
            .expect("Must terminate node 1");
        tracing::info!("Creator submitting create");
        let create_while_vals_are_down_receipt = creator.full_create_claims_flow(create_amount);

        tracing::info!("Redeemer submitting redemption");
        let redeem_while_vals_are_down_receipt =
            redeemer.full_redeem_claims_flow(redemption_amount);

        tracing::info!("Sleeping for {:?}", &DEFAULT_SLEEP_SECS);
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        // Then
        tracing::info!("Verifying creator xand balance hasn't changed");
        let new_creator_xand_bal = creator.get_xand_balance();
        assert_eq!(new_creator_xand_bal, creator_initial_xand_bal);

        tracing::info!("Verifying redeemer bank balance hasn't changed");
        let new_redeemer_bank_bal = redeemer.get_bank_balance();
        assert_eq!(
            new_redeemer_bank_bal,
            redeemer_initial_bank_bal - setup_create_amount
        );

        tracing::info!("Start validator 1 back up again");
        csm.start_node(ContainerInstanceId(1))
            .block_with(&tokio_handle)
            .expect("Must restart node 1");

        tracing::info!("Sleeping for {:?}", &DEFAULT_SLEEP_SECS);
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Asserting Creation request has now been fulfilled");
        creator
            .wait_until_fulfilled_by_trust(&create_while_vals_are_down_receipt)
            .unwrap();

        tracing::info!("Asserting Redemption request has now been fulfilled");
        redeemer
            .wait_until_fulfilled_by_trust(&redeem_while_vals_are_down_receipt)
            .unwrap();
    }

    /// Steps:
    /// 1. Member 0 transfer funds to the trustee with a known id
    /// 2. (Trustee ingests the transaction)
    /// 3. (Fast-forward 24hrs into the future)
    /// 4. Make a create request for the known id
    /// 5. Assert the create request is unfulfilled
    #[test]
    fn transactions_greater_than_24hours_old_will_not_fulfill_a_create_request() {
        logging::init_stdout_logging();

        // Given
        tracing::info!("Starting trustee software");
        let trustee_container = TrusteeContainer::new_with_system_time_offset(HoursOffset::new(0));
        let creator = MemberActorImpl::new(Member::Creator);
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Submit a bank transaction that will expire");
        let id = CorrelationId::gen_random();
        let amount = 12345;
        creator.transfer_funds_for_create(&id, amount);
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Time travel 24 hours into the future!");
        trustee_container.set_system_time_offset(HoursOffset::new(24));
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        // When
        tracing::info!("Submit create request for the expired transaction");
        let receipt = creator.submit_create_request(&id, amount);

        // Then
        tracing::info!(
            "Waiting to ensure create request for expired transaction does not complete"
        );
        let result = creator.wait_until_fulfilled_by_trust(&receipt);
        assert_matches!(
            result,
            Err(MemberActorError::TransactionFulfillmentTimeOutExpired),
            "This unfunded create request should not have been fulfilled!"
        );
    }

    /// 1. Member 0 transfer funds to the trustee with a known id
    /// 2. (Trustee ingests the transaction)
    /// 3. (Fast-forward 23hrs into the future)
    /// 4. Make a create request for the known id
    /// 5. Assert the create request is fulfilled
    #[test]
    fn transactions_less_than_24hours_old_will_fulfill_a_create_request() {
        logging::init_stdout_logging();

        // Given
        tracing::info!("Starting trustee software");
        let trustee_container = TrusteeContainer::new_with_system_time_offset(HoursOffset::new(0));
        let creator = MemberActorImpl::new(Member::Creator);
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Submit a bank transaction");
        let id = CorrelationId::gen_random();
        let amount = 12345;
        creator.transfer_funds_for_create(&id, amount);
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Time travel 23 hours into the future!");
        trustee_container.set_system_time_offset(HoursOffset::new(23));
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        // When
        tracing::info!("Submit create request for the 23hr old transaction");
        let recent_receipt = creator.submit_create_request(&id, amount);

        // Then
        tracing::info!("Waiting for create request for recent transaction to complete");
        creator
            .wait_until_fulfilled_by_trust(&recent_receipt)
            .unwrap();
    }

    #[test]
    fn text_trace_file_contains_logs_periodic_task_triggered_info_event() {
        logging::init_stdout_logging();

        // Given
        tracing::info!("Starting trustee software");
        let trustee_container = TrusteeContainer::default();
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Creating claims");
        let member_0 = MemberActorImpl::new(Member::Default);
        let create_receipt = member_0.full_create_claims_flow(100);
        member_0
            .wait_until_fulfilled_by_trust(&create_receipt)
            .unwrap();

        // Assert
        let traces = trustee_container.get_text_traces();
        let polling_event_count = traces
            .iter()
            .filter(|message| message.contains("PeriodicTaskTriggered"))
            .count();
        assert_that!(polling_event_count > 0);
    }

    #[test]
    fn open_telemetry_trace_file_contains_polling_debug_event() {
        logging::init_stdout_logging();

        // Given
        tracing::info!("Starting trustee software");
        let trustee_container = TrusteeContainer::default();
        std::thread::sleep(DEFAULT_SLEEP_SECS);

        tracing::info!("Creating claims");
        let member_0 = MemberActorImpl::new(Member::Default);
        let create_receipt = member_0.full_create_claims_flow(100);
        member_0
            .wait_until_fulfilled_by_trust(&create_receipt)
            .unwrap();

        // Assert
        let traces = trustee_container.get_open_telemetry_traces();
        let polling_event_count = traces
            .iter()
            .filter(|message| message.contains("Polling"))
            .count();
        assert_that!(polling_event_count > 0);
    }
}
