#![forbid(unsafe_code)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

#[macro_use]
extern crate derive_more;
#[macro_use]
extern crate snafu;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;

pub mod async_adapter;
mod bank_dispatcher;
mod banks_config;
mod config_reader;
pub mod core;
mod create_processor;
mod engine;
mod errors;
mod logging_events;
mod metrics;
mod poller;
mod redeem_processor;
mod secret_store;
mod status;
pub mod status_store;
mod trust_dispatcher;
mod txn_issuer;
mod xand_api_config;

mod history_fetcher;
#[cfg(test)]
mod test_utils;

use crate::{
    async_adapter::adapters::tokio_adapter::TokioAdapter,
    core::{ServicesChecker, TrustBankDispatcher, TxnIssuer},
    create_processor::InMemoryCreateProcessor,
    engine::TrusteeEngine,
    errors::TrustError,
    history_fetcher::BtrHistoryFetcher,
    logging_events::LoggingEvent,
    poller::Poller,
    redeem_processor::InMemoryRedeemProcessor,
    status::CheckServices,
    trust_dispatcher::TrustDispatcher,
    txn_issuer::XandIssuer,
};
use bank_transaction_repository::{
    construct_store_client, BankTransactionRepository, BankTransactionRepositoryConfiguration,
};
use config_reader::ConfigLoaderError;
use std::{
    path::Path,
    sync::{atomic::AtomicBool, Arc},
    time::Duration,
};
use tokio::runtime::Runtime;
use trustee_node_config::TrustConfigValidationError;
use xand_address::Address;
use xand_api_config::XandApiConfig;

type Result<T, E = TrustError> = std::result::Result<T, E>;

pub enum MissingDatabaseAction {
    CreateDatabase,
    RaiseError,
}

/// Starts the trust node, runs forever unless `validate_conf` is passed, in which case it returns
/// immediately after validating the configuration.
/// The `stop` flag indicates if the trustee should stop.
pub fn start_trust(
    config_path: &str,
    validate_conf: bool,
    missing_database_action: MissingDatabaseAction,
    stop: &AtomicBool,
) -> Result<(), TrustError> {
    let res = _start_trust(config_path, validate_conf, missing_database_action, stop);
    if let Err(ref e) = res {
        tracing::error!(message = ?LoggingEvent::TrustError { err: e.clone() })
    };
    res
}

// Hides the real start just to provide an easy way to log all errors
fn _start_trust(
    config_path: &str,
    validate_conf: bool,
    missing_database_action: MissingDatabaseAction,
    stop: &AtomicBool,
) -> Result<(), TrustError> {
    let rt = Arc::new(Runtime::new().map_err(|e| TrustError::RuntimeError {
        message: e.to_string(),
    })?);
    let cfg = Arc::new(config_reader::config_reader(config_path)?);
    println!("{:#?}", &cfg);

    verify_database_exists(&cfg, missing_database_action)?;

    if cfg.banks.is_empty() {
        tracing::warn!(message = ?LoggingEvent::NoBanksInConfig);
    }
    let xand_api_config = XandApiConfig::from_trust_config(&cfg)?;

    let btr_config = BankTransactionRepositoryConfiguration::from(cfg.as_ref().clone());

    let store_client = construct_store_client(&cfg.transaction_db_path)
        .map_err(bank_transaction_repository::error::Error::from)?;

    let btr =
        BankTransactionRepository::initialize(btr_config, store_client.clone(), store_client)?;

    let tokio_driver = TokioAdapter::get().map_err(|source| TrustError::Async { source })?;
    let history_fetcher = BtrHistoryFetcher::new(rt.clone(), btr.clone());
    let dispatcher = TrustDispatcher::new(cfg.clone(), tokio_driver, history_fetcher)?;
    let trust_addr: Address =
        cfg.trust_address
            .parse()
            .map_err(|_| ConfigLoaderError::ValidationError {
                source: TrustConfigValidationError::BadConfigValue {
                    msg: format!("{} is not a valid address", cfg.trust_address),
                },
            })?;

    let txn_issuer = XandIssuer::new(rt.clone(), xand_api_config.clone(), trust_addr)?;

    let poller = new_poller_with_dependencies(
        rt.clone(),
        cfg.redeem_retry_delay,
        xand_api_config,
        &txn_issuer,
        &dispatcher,
    )?;
    let service_checker = CheckServices::new(poller, &dispatcher);

    if validate_conf {
        return service_checker
            .check_services()
            .map_err(|report| TrustError::HealthCheckError { report });
    }

    let mut engine = TrusteeEngine::new(
        cfg.polling_delay,
        cfg.iterations_for_status_check,
        service_checker,
    );

    let _rt_guard = rt.enter(); // periodic synchronization requires a tokio context
    let _sync_handle = btr.start_periodic_synchronization();
    engine.run(stop)
}

fn verify_database_exists(
    cfg: &Arc<trustee_node_config::TrusteeConfig>,
    create_database: MissingDatabaseAction,
) -> Result<(), TrustError> {
    match create_database {
        MissingDatabaseAction::CreateDatabase => {
            if let Some(parent) = Path::new(&cfg.transaction_db_path).parent() {
                if !parent.exists() {
                    std::fs::create_dir_all(parent).map_err(|e| {
                        TrustError::DbInitializationError {
                            message: e.to_string(),
                            path: cfg.transaction_db_path.clone(),
                        }
                    })?
                }
            }
        }
        MissingDatabaseAction::RaiseError => {
            if !Path::new(&cfg.transaction_db_path).exists() {
                return Err(TrustError::DbInitializationError {
                    message: "Database does not exist.".into(),
                    path: cfg.transaction_db_path.clone(),
                });
            }
        }
    }
    Ok(())
}

type InMemPoller<'a, BD, T> =
    Poller<InMemoryCreateProcessor<'a, BD, T>, InMemoryRedeemProcessor<'a, BD, T>>;
/// Creates a new poller along with its dependencies in order
/// to allow for reuse of those dependencies.
fn new_poller_with_dependencies<'a, BD: TrustBankDispatcher, T: TxnIssuer>(
    rt: Arc<Runtime>,
    redeem_retry_delay: u64,
    xand_api_config: XandApiConfig,
    txn_issuer: &'a T,
    dispatcher: &'a BD,
) -> Result<InMemPoller<'a, BD, T>, TrustError> {
    // TODO: The poller also instantiates a xand client which is redundant with the one
    //   in the tx issuer. Combine.
    let create_processor = InMemoryCreateProcessor::new(dispatcher, txn_issuer);
    let redeem_processor = InMemoryRedeemProcessor::new(
        dispatcher,
        txn_issuer,
        Duration::from_secs(redeem_retry_delay),
    );
    let poller = Poller::new(rt, xand_api_config, create_processor, redeem_processor)?;
    Ok(poller)
}
