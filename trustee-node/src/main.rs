#![forbid(unsafe_code)]

use clap::{App, Arg};
use std::process;
use trustee_node::{start_trust, MissingDatabaseAction};

mod tracing;

fn main() {
    let version = env!("CARGO_PKG_VERSION");

    let matches = App::new("XAND Trustee")
        .version(version)
        .about("Processes XAND net create requests and fulfills redeems")
        .arg(
            Arg::with_name("config_path")
                .value_name("config_path")
                .required(true)
                .takes_value(true)
                .help("Set path to directory containing config files i.e. /some/place/with/config"),
        )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .short("v")
                .multiple(true)
                .help("increase output verbosity"),
        )
        .arg(
            Arg::with_name("validate_conf")
                .long("validate-conf")
                .short("c")
                .help("validate the configuration by making read requests to each service."),
        )
        .arg(
            Arg::with_name("create_db")
                .long("allow-create-db")
                .short("d")
                .help("Create the transaction database if it doesn't exist."),
        )
        .get_matches();

    let config_path = matches.value_of("config_path").unwrap();
    let validate_conf = matches.is_present("validate_conf");
    let missing_db_action = match matches.is_present("create_db") {
        true => MissingDatabaseAction::CreateDatabase,
        false => MissingDatabaseAction::RaiseError,
    };

    let _tracing_guard = tracing::try_init()
        .expect("Must be able to init global tracer and logger before starting trust application.");

    // If many more options are added to the arg parsing, consider using
    // https://docs.rs/structopt/0.2.15/structopt/
    if start_trust(
        config_path,
        validate_conf,
        missing_db_action,
        &Default::default(),
    )
    .is_err()
    {
        process::exit(1);
    }
}
