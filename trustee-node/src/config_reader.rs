use std::{
    path::{Path, PathBuf},
    sync::Arc,
};

use config::{Config, Environment, File as CfgFile, FileSourceFile};
use glob::{glob, PatternError};

use trustee_node_config::{SecretStoreConfig, TrustConfigValidationError, TrusteeConfig};
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

#[derive(Debug, Snafu, Clone, Serialize)]
pub enum ConfigLoaderError {
    #[snafu(display("Configuration source path does not exist: {:?}", path))]
    PathNotFound { path: String },
    #[snafu(display("No config files found at path {}", path))]
    NoConfigFiles { path: String },
    #[snafu(display("Unable to deserialize configuration {:?}: {:?}", path, source))]
    ConfigDeserializationError {
        path: String,
        #[snafu(source(from(config::ConfigError, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<config::ConfigError>,
    },
    #[snafu(display("Incorrectly constructed glob pattern"))]
    GlobPatternError,
    #[snafu(display("Config validation failed: {}", source))]
    ValidationError { source: TrustConfigValidationError },
    #[snafu(display("Unable to load config: {:?}", source))]
    InternalLoaderError {
        #[snafu(source(from(config::ConfigError, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<config::ConfigError>,
    },
}

impl From<PatternError> for ConfigLoaderError {
    fn from(_: PatternError) -> Self {
        ConfigLoaderError::GlobPatternError
    }
}

impl From<config::ConfigError> for ConfigLoaderError {
    fn from(e: config::ConfigError) -> Self {
        ConfigLoaderError::InternalLoaderError {
            source: Arc::new(e),
        }
    }
}

pub fn config_reader(config_path: &str) -> Result<TrusteeConfig, ConfigLoaderError> {
    let absolute_config_path =
        std::fs::canonicalize(config_path).map_err(|_| ConfigLoaderError::PathNotFound {
            path: config_path.to_string(),
        })?;
    let mut config = Config::default();

    if !absolute_config_path.exists() {
        return Err(ConfigLoaderError::PathNotFound {
            path: config_path.to_string(),
        });
    }

    let config_files = grab_configs_in_path(&config_path)?;

    if config_files.is_empty() {
        return Err(ConfigLoaderError::NoConfigFiles {
            path: config_path.to_string(),
        });
    }

    config.merge(config_files)?;
    config.merge(Environment::with_prefix("trustee_node").separator("-"))?;

    let mut cfg: TrusteeConfig = serde_path_to_error::deserialize(config).map_err(|err| {
        ConfigLoaderError::ConfigDeserializationError {
            path: err.path().to_string(),
            source: Arc::new(err.into_inner()),
        }
    })?;

    cfg.secret_store =
        make_absolute_secret_store_config(absolute_config_path.clone(), cfg.secret_store);
    cfg.transaction_db_path =
        build_config_relative_path(absolute_config_path, cfg.transaction_db_path)
            .to_string_lossy()
            .to_string();

    cfg.validate_trustee_config()
        .map_err(|source| ConfigLoaderError::ValidationError { source })?;

    Ok(cfg)
}

fn make_absolute_secret_store_config<P: AsRef<Path>>(
    config_dir_path: P,
    config: SecretStoreConfig,
) -> SecretStoreConfig {
    match config {
        SecretStoreConfig::LocalFile(config) => {
            SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
                yaml_file_path: build_config_relative_path(config_dir_path, config.yaml_file_path)
                    .to_string_lossy()
                    .to_string(),
            })
        }
    }
}

fn grab_configs_in_path(config_path: &&str) -> Result<Vec<CfgFile<FileSourceFile>>, PatternError> {
    Ok(glob(&format!("{}/*.toml", config_path))?
        .chain(glob(&format!("{}/*.yaml", config_path))?)
        .chain(glob(&format!("{}/*.yml", config_path))?)
        .chain(glob(&format!("{}/*.json", config_path))?)
        .map(|path| CfgFile::from(path.unwrap()))
        .collect())
}

fn build_config_relative_path<C: AsRef<Path>, P: AsRef<Path>>(
    config_dir_path: C,
    path: P,
) -> PathBuf {
    [config_dir_path.as_ref(), path.as_ref()].iter().collect()
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use assert_matches::assert_matches;
    use itertools::Itertools;
    use std::{
        fs::File,
        io::Write,
        path::{Path, PathBuf},
    };

    use super::build_config_relative_path;

    use tempfile::{tempdir, TempDir};
    use trustee_node_config::trust_banks::TrustBanksConfig;

    use super::config_reader;

    const LOCAL_CONFIG_DIR_REL_PATH: &str = "../config";
    const OK_CFG_DEFAULTS: &str = r#"
            xand_api_endpoint: "http://localhost:8008"
            trust_address: "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7"
            access_token_folder: "./"
            polling_delay: 0
            iterations_for_status_check: 1
            redeem_retry_delay: 3

            banks: []

            secret_store:
                local_file:
                    yaml-file-path: "./secrets.yaml"

            transaction_db_path: "./db/test_db.sqlite3"

            sync_policy:
              cooldown_timeout_secs: 60
              periodic_synchronization_interval_secs: 3600
        "#;

    fn create_yaml_config_file(dir: &TempDir, config_string: &str) {
        let file_path = dir.path().join("config.yaml");
        let mut file = File::create(file_path).unwrap();
        writeln!(file, "{}", config_string).unwrap();

        File::create(dir.path().join("secrets.yaml")).unwrap();
    }

    #[test]
    fn can_read_global_config() {
        let trustee_config = config_reader(LOCAL_CONFIG_DIR_REL_PATH).unwrap();
        assert_eq!(5, trustee_config.polling_delay);
    }

    #[test]
    fn config_dir_must_exist() {
        let res = config_reader("configgyyyy");
        assert!(res.is_err());
        assert_eq!(
            "Configuration source path does not exist: \"configgyyyy\"",
            res.unwrap_err().to_string()
        );
    }

    #[test]
    fn config_files_must_exist() {
        let dir = tempdir().unwrap();
        let res = config_reader(dir.path().to_str().unwrap());
        assert!(res.is_err());
        assert!(res
            .unwrap_err()
            .to_string()
            .starts_with("No config files found at path"));
    }
    #[test]
    fn mcb_deserialization() {
        let dir = tempdir().unwrap();

        let config_str = OK_CFG_DEFAULTS.to_owned()
            + r#"
            banks:
                - mcb:
                    routing_number: "000111222"
                    bank_api_url: "http://localhost"
                    trust_account: ""
                    secret_key_username: ""
                    secret_key_password: ""
                    secret_key_client_app_ident: ""
                    secret_key_organization_id: ""
                    display_name: "mcb"

        "#;

        create_yaml_config_file(&dir, &config_str);

        let cfg = config_reader(dir.path().to_str().unwrap()).unwrap();

        let actual_banks: Vec<&TrustBanksConfig> = cfg.banks.iter().collect();
        assert_eq!(actual_banks.len(), 1);
        assert_matches!(actual_banks[0], TrustBanksConfig::Mcb(_));
    }

    #[test]
    fn both_banks_deserialize() {
        let dir = tempdir().unwrap();

        let config_str = OK_CFG_DEFAULTS.to_owned()
            + r#"
            banks:
                - treasury_prime:
                    routing_number: "111222333"
                    bank_api_url: "http://localhost"
                    trust_account: ""
                    secret_key_username: "imakeylol"
                    secret_key_password: "suchkeyvaluewow"
                    display_name: "provident"
                - mcb:
                    routing_number: "000111222"
                    bank_api_url: "http://localhost"
                    trust_account: ""
                    secret_key_username: ""
                    secret_key_password: ""
                    secret_key_client_app_ident: ""
                    secret_key_organization_id: ""
                    display_name: "mcb"

        "#;

        create_yaml_config_file(&dir, &config_str);

        let cfg = config_reader(dir.path().to_str().unwrap()).unwrap();

        let actual_banks: Vec<&TrustBanksConfig> = cfg.banks.iter().sorted().collect();
        assert_eq!(actual_banks.len(), 2);
        assert_matches!(actual_banks[0], TrustBanksConfig::Mcb(_));
        assert_matches!(actual_banks[1], TrustBanksConfig::TreasuryPrime(_));
    }

    #[test]
    fn required_fields_must_exist() {
        let dir = tempdir().unwrap();

        let config_str = r#"
            trust_address: "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7"
            access_token_folder: "./"
            polling_delay: 0
            iterations_for_status_check: 1
            redeem_retry_delay: 300
        "#;

        create_yaml_config_file(&dir, config_str);

        let err = config_reader(dir.path().to_str().unwrap());
        assert!(err.is_err());
        assert!(err
            .unwrap_err()
            .to_string()
            .ends_with("missing field `xand_api_endpoint`"));
    }

    #[test]
    fn correct_types_in_config() {
        let dir = tempdir().unwrap();

        let config_str = r#"
            xand_api_endpoint: "http://localhost:8008"
            trust_address: "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7"
            access_token_folder: "./"
            polling_delay: "uh oh not a number!!!!"
            iterations_for_status_check: 1
            redeem_retry_delay: 300
        "#;

        create_yaml_config_file(&dir, config_str);

        let err = config_reader(dir.path().to_str().unwrap());
        assert!(err.is_err());

        let message = err.unwrap_err().to_string();
        assert!(message.starts_with("Unable to deserialize configuration \"polling_delay\": invalid type: string \"uh oh not a number!!!!\", expected an integer for key `polling_delay` in"), "Actual message: {}", message);
    }

    #[test]
    fn environment_var_takes_precedence_over_files() {
        let dir = tempdir().unwrap();

        create_yaml_config_file(&dir, OK_CFG_DEFAULTS);

        std::env::set_var("TRUSTEE_NODE_trust_address", "wooooo");

        let result = config_reader(dir.path().to_str().unwrap());
        std::env::remove_var("TRUSTEE_NODE_trust_address");

        assert_eq!(result.unwrap().trust_address, "wooooo");
    }

    #[test]
    fn json_takes_precedence_over_yaml() {
        let dir = tempdir().unwrap();

        create_yaml_config_file(&dir, OK_CFG_DEFAULTS);

        let file_path = dir.path().join("config.json");
        let mut file = File::create(file_path).unwrap();
        let config_json = r#"
        {
            "xand_api_endpoint": "http://stonksgoup.com"
        }
        "#;

        writeln!(file, "{}", config_json).unwrap();

        let config = config_reader(dir.path().to_str().unwrap()).unwrap();

        assert_eq!(
            config.xand_api_endpoint,
            "http://stonksgoup.com".parse().unwrap()
        )
    }

    #[test]
    fn config_reader__makes_secret_store_path_absolute() {
        let dir = tempdir().unwrap();

        create_yaml_config_file(&dir, OK_CFG_DEFAULTS);

        let config = config_reader(dir.path().to_str().unwrap()).unwrap();

        let trustee_node_config::SecretStoreConfig::LocalFile(secret_store_config) =
            config.secret_store;

        assert_eq!(
            Path::new(&secret_store_config.yaml_file_path),
            dir.path().join("secrets.yaml")
        )
    }

    #[test]
    fn config_reader__makes_transaction_db_path_absolute() {
        let dir = tempdir().unwrap();

        create_yaml_config_file(&dir, OK_CFG_DEFAULTS);

        let config = config_reader(dir.path().to_str().unwrap()).unwrap();

        assert_eq!(
            Path::new(&config.transaction_db_path),
            dir.path().join("db/test_db.sqlite3")
        )
    }

    #[test]
    fn build_config_relative_path__works_with_relative_second_part() {
        let root = "/config/dir/";
        let file = "./foo/secrets.yaml";

        let abs = build_config_relative_path(root, file);

        assert_eq!(abs, PathBuf::from("/config/dir/foo/secrets.yaml"));
    }

    #[test]
    fn build_config_relative_path__works_with_plain_file_name() {
        let root = "/config/dir/";
        let file = "secrets.yaml";

        let abs = build_config_relative_path(root, file);

        assert_eq!(abs, PathBuf::from("/config/dir/secrets.yaml"));
    }

    #[test]
    fn build_config_relative_path__works_with_omitted_trailing_slash() {
        let root = "/config/dir";
        let file = "./secrets.yaml";

        let abs = build_config_relative_path(root, file);

        assert_eq!(abs, PathBuf::from("/config/dir/secrets.yaml"));
    }

    #[test]
    fn build_config_relative_path__works_with_absolute_second_part() {
        let root = "/config/dir/";
        let file = "/some/absolute/path/secrets.yaml";

        let abs = build_config_relative_path(root, file);

        assert_eq!(abs, PathBuf::from("/some/absolute/path/secrets.yaml"));
    }
}
