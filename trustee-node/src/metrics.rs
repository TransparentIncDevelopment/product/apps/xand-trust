use crate::logging_events::LoggingEvent::MetricsError;
use num_traits::cast::ToPrimitive;
use pipesrv_emitter::spin_up_emitter;
use std::sync::Once;
use xand_api_client::{PendingCreateRequest, PendingRedeemRequest};
use xand_banks::{models::BankBalance, xand_money::Money};
use xand_logging_metrics::LoggingMetricsBag;
use xand_metrics::{DimensionedGaugeDef, GaugeDef, MetricOpts, Metrics, MetricsBag};
use xand_metrics_prometheus::PromMetrics;

pub(crate) const AVAILABLE_BALANCE_DIM: &str = "available_balance";
pub(crate) const CURRENT_BALANCE_DIM: &str = "current_balance";

pub(crate) fn init_metrics() -> TrustMetricsBag {
    let mut trust_metrics_bag = TrustMetricsBag::default();
    let metrics = PromMetrics::default();
    metrics.initialize(&mut trust_metrics_bag).unwrap();
    // It is ok to skip this in tests that create additional `Poller` instances as tests do not need
    // to send metrics to any backend.
    static REGISTRATION: Once = Once::new();
    REGISTRATION.call_once(|| {
        LoggingMetricsBag::init(&metrics).unwrap();
        spin_up_emitter(5, Box::new(metrics));
    });
    trust_metrics_bag
}

#[derive(MetricsBag)]
pub(crate) struct TrustMetricsBag {
    pub(crate) outstanding_creates: GaugeDef,
    pub(crate) outstanding_redeems: GaugeDef,
    pub(crate) bank_balances: DimensionedGaugeDef,
    pub(crate) total_claims: GaugeDef,
}

impl TrustMetricsBag {
    #[cfg(test)]
    pub(crate) fn new() -> Self {
        let mut trust_metrics_bag = TrustMetricsBag::default();
        PromMetrics::default()
            .initialize(&mut trust_metrics_bag)
            .unwrap();
        trust_metrics_bag
    }
}

impl Default for TrustMetricsBag {
    fn default() -> Self {
        Self {
            outstanding_creates: GaugeDef::new(
                "outstanding_creates",
                "Track currently outstanding create and redeem requests",
            ),
            outstanding_redeems: GaugeDef::new(
                "outstanding_redeems",
                "Track currently outstanding create and redeem requests",
            ),
            bank_balances: DimensionedGaugeDef::new(
                vec!["bank_routing_number", "balance_type"],
                MetricOpts {
                    name: "bank_balances".to_string(),
                    desc: "Tracks balances of trust accounts".to_string(),
                },
            ),
            total_claims: GaugeDef::new(
                "total_claims",
                "Tracks all claims that exist currently on the network",
            ),
        }
    }
}

pub(crate) enum OutstandingFiatReqs<'a> {
    Creates(&'a Vec<PendingCreateRequest>),
    Redeems(&'a Vec<PendingRedeemRequest>),
}
pub(crate) fn count_outstanding(metrics: &TrustMetricsBag, outstanding: OutstandingFiatReqs) {
    match outstanding {
        OutstandingFiatReqs::Creates(creates) => {
            metrics.outstanding_creates.set(creates.len() as f64)
        }
        OutstandingFiatReqs::Redeems(redeems) => {
            metrics.outstanding_redeems.set(redeems.len() as f64)
        }
    }
}

pub(crate) fn record_bank_balance(
    metrics: &TrustMetricsBag,
    bank_routing_number: &str,
    balance: &BankBalance,
) {
    match balance.current_balance.into_major_units().to_f64() {
        Some(x) => rec_specific_balance(metrics, bank_routing_number, CURRENT_BALANCE_DIM, x),
        None => tracing::error!(message = ?MetricsError {
            msg: format!(
                "Was unable to convert current balance of {} to f64: {:?}",
                bank_routing_number, balance
            )
        }),
    };
    match balance.available_balance.into_major_units().to_f64() {
        Some(x) => rec_specific_balance(metrics, bank_routing_number, AVAILABLE_BALANCE_DIM, x),
        None => tracing::error!(message = ?MetricsError {
            msg: format!(
                "Was unable to convert available balance of {} to f64: {:?}",
                bank_routing_number, balance
            )
        }),
    };
}

fn rec_specific_balance(metrics: &TrustMetricsBag, bank_name: &str, bal_type: &str, bal: f64) {
    match metrics.bank_balances.with_dims(vec![bank_name, bal_type]) {
        Ok(g) => g.set(bal),
        Err(e) => {
            tracing::error!(message = ?MetricsError {
                msg: format!(
                    "Invalid dimension settings while recording bank balance: {}",
                    e
                )
            });
        }
    }
}

pub(crate) fn record_claims(metrics: &TrustMetricsBag, total_claims: u64) {
    metrics.total_claims.set(total_claims as f64);
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test_utils::test_val::TestVal;
    use xand_api_client::{BankAccountId, CorrelationId};
    use xand_banks::xand_money::Usd;

    #[allow(clippy::float_cmp)]
    #[test]
    fn sets_gauge_appropriately() {
        let mut creates = vec![];
        let mut redeems = vec![];
        let metrics = TrustMetricsBag::new();

        macro_rules! count_and_assert {
            ($createc:expr, $redeemc:expr) => {
                count_outstanding(&metrics, OutstandingFiatReqs::Creates(&creates));
                count_outstanding(&metrics, OutstandingFiatReqs::Redeems(&redeems));

                assert_eq!(metrics.outstanding_creates.get(), $createc);
                assert_eq!(metrics.outstanding_redeems.get(), $redeemc);
            };
        }

        count_and_assert!(0., 0.);

        let correlation_id = CorrelationId::test_val();
        let bank_account = BankAccountId {
            routing_number: "routing".to_string(),
            account_number: "account".to_string(),
        };
        let fakecreate = PendingCreateRequest {
            correlation_id: correlation_id.clone(),
            amount_in_minor_unit: 5000,
            account: bank_account.clone().into(),
            completing_transaction: None,
        };
        let fakeredeem = PendingRedeemRequest {
            correlation_id,
            amount_in_minor_unit: 5000,
            account: bank_account.into(),
            completing_transaction: None,
        };

        creates.push(fakecreate);
        count_and_assert!(1., 0.);

        redeems.push(fakeredeem.clone());
        redeems.push(fakeredeem);
        count_and_assert!(1., 2.);

        creates.clear();
        redeems.clear();
        count_and_assert!(0., 0.);
    }

    #[allow(clippy::float_cmp)]
    #[test]
    fn records_balance_properly() {
        let metrics = TrustMetricsBag::new();
        record_bank_balance(
            &metrics,
            "bank1",
            &BankBalance::new(
                Usd::from_i64_minor_units(5_00).unwrap(),
                Usd::from_i64_minor_units(10_00).unwrap(),
            ),
        );
        record_bank_balance(
            &metrics,
            "bank2",
            &BankBalance::new(
                Usd::from_i64_minor_units(6_00).unwrap(),
                Usd::from_i64_minor_units(11_00).unwrap(),
            ),
        );
        assert_eq!(
            metrics
                .bank_balances
                .with_dims(vec!["bank1", AVAILABLE_BALANCE_DIM])
                .unwrap()
                .get(),
            5.0
        );
        assert_eq!(
            metrics
                .bank_balances
                .with_dims(vec!["bank1", CURRENT_BALANCE_DIM])
                .unwrap()
                .get(),
            10.0
        );
        assert_eq!(
            metrics
                .bank_balances
                .with_dims(vec!["bank2", AVAILABLE_BALANCE_DIM])
                .unwrap()
                .get(),
            6.0
        );
        assert_eq!(
            metrics
                .bank_balances
                .with_dims(vec!["bank2", CURRENT_BALANCE_DIM])
                .unwrap()
                .get(),
            11.0
        );
    }
}
