use crate::{
    async_adapter::ports::{future_resolver::FutureResolver, sync_future_ext::SyncFutureExt},
    bank_dispatcher::{create_dispatcher, BankDispatcher, BankDispatcherErrors},
    banks_config::TrustBanksCreator,
    core::{BankError, TrustBankDispatcher, *},
    history_fetcher::HistoryFetcher,
    logging_events::LoggingEvent,
    secret_store::create_secret_store,
};
use chrono::Duration;
use snafu::ResultExt;
use std::{collections::HashMap, sync::Arc};
use trustee_node_config::{Account, TrusteeConfig};
use xand_api_client::{CorrelationId, PendingCreateRequest, PendingRedeemRequest};
use xand_banks::{
    date_range::DateRange,
    models::{account::TransferRequest, BankBalance, BankTransaction, BankTransactionType},
    xand_money::{Money, Usd},
};

pub struct TrustDispatcher<'a, FutRt: FutureResolver, Fetcher: HistoryFetcher> {
    history_duration: Duration,
    dispatcher: Box<dyn BankDispatcher + 'a>,
    cfg: Arc<TrusteeConfig>,
    runtime_access: FutRt,
    history_fetcher: Fetcher,
}

impl<FutRt: FutureResolver, Fetcher: HistoryFetcher> TrustDispatcher<'_, FutRt, Fetcher> {
    pub fn new(
        cfg: Arc<TrusteeConfig>,
        runtime_access: FutRt,
        history_fetcher: Fetcher,
    ) -> Result<Self, crate::errors::TrustError> {
        let secret_store = create_secret_store(&cfg.secret_store);

        let xand_dispatcher = create_dispatcher::<TrustBanksCreator>(
            &TrustBanksCreator {
                configs: cfg.banks.clone(),
            },
            secret_store,
        )?;

        Ok(Self {
            history_duration: Duration::days(1),
            cfg,
            dispatcher: Box::new(xand_dispatcher),
            runtime_access,
            history_fetcher,
        })
    }

    fn get_trust_account_for_member_account(&self, account: Account) -> Result<Account, BankError> {
        let bank = self
            .dispatcher
            .get_bank_by_routing_number(&account.routing_number)?;
        let trust_account = Account {
            account_number: bank.reserve_account(),
            routing_number: bank.routing_number(),
        };
        Ok(trust_account)
    }
}

impl<FutRt: FutureResolver, Fetcher: HistoryFetcher> TrustDispatcher<'_, FutRt, Fetcher> {
    fn expected_txn_exists_in_history(
        &self,
        history_data: Vec<BankTransaction>,
        amount: Usd,
        id: &CorrelationId,
        txn_type: BankTransactionType,
    ) -> bool {
        let id_str = id.to_string();
        let trimmed_id_str = trim_0x_prefix(&id_str);

        history_data.iter().any(|tx| {
            let trimmed_metadata = trim_0x_prefix(&tx.metadata);
            tx.amount == amount && trimmed_metadata == trimmed_id_str && tx.txn_type == txn_type
        })
    }
}

impl<FutRt: FutureResolver, Fetcher: HistoryFetcher> TrustBankDispatcher
    for TrustDispatcher<'_, FutRt, Fetcher>
{
    fn transfer_exists_for_create(
        &self,
        pending_create: PendingCreateRequest,
        account: Account,
    ) -> Result<bool, BankError> {
        let create_amount = pending_create.amount_in_minor_unit;
        let create_id = pending_create.correlation_id;
        tracing::info!(message = ?LoggingEvent::LookingForMatchingTxnForCreate {
            id: create_id.clone(),
            amount: create_amount,
            bank_routing_number: account.routing_number.clone()
        });

        let date_range =
            DateRange::from_past_to_now(self.history_duration).context(DurationError)?;
        let trust_account = self.get_trust_account_for_member_account(account)?;
        let history = self
            .history_fetcher
            .get_history(trust_account, date_range)?;

        let create_amount = Usd::from_u64_minor_units(create_amount)?;
        Ok(self.expected_txn_exists_in_history(
            history,
            create_amount,
            &create_id,
            BankTransactionType::Credit,
        ))
    }

    fn transfer_exists_for_redeem(
        &self,
        redeem: PendingRedeemRequest,
        account: Account,
    ) -> Result<bool, BankError> {
        let redeem_amount = redeem.amount_in_minor_unit;
        let redeem_id = redeem.correlation_id;

        let date_range =
            DateRange::from_past_to_now(self.history_duration).context(HistoryError)?;

        let bank = self
            .dispatcher
            .get_bank_by_routing_number(&account.routing_number)?;

        let history = bank
            .history(&bank.reserve_account(), date_range)
            .block_with(&self.runtime_access)
            .context(HistoryError)?;

        let redeem_amount = Usd::from_u64_minor_units(redeem_amount)?;
        Ok(self.expected_txn_exists_in_history(
            history,
            redeem_amount,
            &redeem_id,
            BankTransactionType::Debit,
        ))
    }

    fn bank_exists(&self, account: &Account) -> Result<bool, BankError> {
        let bank = self
            .dispatcher
            .get_bank_by_routing_number(&account.routing_number);
        match bank {
            Ok(_) => Ok(true),
            Err(e) => match e {
                BankDispatcherErrors::BankNotFound { .. } => Ok(false),
                _ => Err(e.into()),
            },
        }
    }

    fn transfer(
        &self,
        amount: u64,
        to_account: Account,
        id: &CorrelationId,
    ) -> Result<(), BankError> {
        let bank = self
            .dispatcher
            .get_bank_by_routing_number(&to_account.routing_number)?;
        // Check that there is sufficient balance to perform the transfer
        let bal = bank
            .balance(&bank.reserve_account())
            .block_with(&self.runtime_access)
            .context(BalanceError)?;
        let converted = bal.available_balance.into_u64_minor_units()?;
        if converted < amount {
            return Err(BankError::InsufficientBalance);
        }

        let id_str = id.to_string();
        let trimmed_id_str = trim_0x_prefix(&id_str);

        let amount = Usd::from_u64_minor_units(amount)?;

        let tr = TransferRequest {
            src_account_number: bank.reserve_account(),
            dst_account_number: to_account.account_number,
            amount,
        };
        bank.transfer(tr, Some(trimmed_id_str))
            .block_with(&self.runtime_access)
            .map(|_| {})
            .context(TransferError)
    }

    fn reserve_balances(&self) -> HashMap<String, Result<BankBalance, BankError>> {
        let mut balances = HashMap::new();
        for bank in &self.dispatcher.banks() {
            balances.insert(
                bank.routing_number(),
                self.reserve_balance_for(&bank.routing_number()),
            );
        }
        balances
    }

    fn reserve_balance_for(&self, routing_num: &str) -> Result<BankBalance, BankError> {
        let bank = self.dispatcher.get_bank_by_routing_number(routing_num)?;
        bank.balance(&bank.reserve_account())
            .block_with(&self.runtime_access)
            .context(BalanceError)
    }

    fn is_allowlisted(&self, account: &Account) -> bool {
        self.cfg.partner_accounts.is_allowlisted(account)
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use super::*;
    use crate::{
        async_adapter::adapters::tokio_adapter::TokioAdapter, history_fetcher::HistoryFetchError,
    };
    use bank_transaction_repository::SynchronizationPolicyConfiguration;
    use pseudo::Mock;
    use std::collections::HashSet;
    use std::{io::Write, str::FromStr};
    use tempfile::TempDir;
    use trustee_node_config::{PartnerAccounts, SecretStoreConfig};
    use url::Url;
    use xand_api_client::{BankAccountId, BankAccountInfo, CorrelationId};
    use xand_banks::{
        errors::XandBanksErrors,
        models::{account::DetailedAccount, BankTransactionType, BankTransferResponse},
        Bank,
    };
    use xand_secrets::ExposeSecret;
    use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

    impl TrustDispatcher<'_, TokioAdapter, MockHistoryFetcher> {
        fn test_with_bank(
            mock_bank: &MockBank,
        ) -> TrustDispatcher<TokioAdapter, MockHistoryFetcher> {
            let mock_bank_dispatcher = MockBankDispatcher {
                get_bank_by_routing_number: Mock::new(Ok(mock_bank as &dyn Bank)),
            };

            TrustDispatcher {
                history_duration: Duration::days(1),
                dispatcher: Box::new(mock_bank_dispatcher),
                cfg: Arc::new(create_test_config()),
                runtime_access: TokioAdapter::get().unwrap(),
                history_fetcher: MockHistoryFetcher::new(),
            }
        }
    }

    #[derive(Clone)]
    struct MockBank {
        pub balance: Mock<String, Result<BankBalance, XandBanksErrors>>,
        pub transfer:
            Mock<(TransferRequest, Option<String>), Result<BankTransferResponse, XandBanksErrors>>,
        pub routing_number: Mock<(), String>,
        pub reserve_account: Mock<(), String>,
        pub history: Mock<(String, DateRange), Result<Vec<BankTransaction>, XandBanksErrors>>,
    }

    #[async_trait::async_trait]
    impl Bank for MockBank {
        fn routing_number(&self) -> String {
            self.routing_number.call(())
        }

        fn reserve_account(&self) -> String {
            self.reserve_account.call(())
        }

        fn accounts(&self) -> Vec<DetailedAccount> {
            unimplemented!()
        }

        fn get_enriched_account(&self, _: &str) -> Result<DetailedAccount, XandBanksErrors> {
            unimplemented!()
        }

        fn display_name(&self) -> String {
            unimplemented!()
        }

        fn memo_char_limit(&self) -> u32 {
            unimplemented!()
        }

        async fn balance(&self, account_number: &str) -> Result<BankBalance, XandBanksErrors> {
            self.balance.call(account_number.into())
        }

        async fn transfer(
            &self,
            request: TransferRequest,
            metadata: Option<String>,
        ) -> Result<BankTransferResponse, XandBanksErrors> {
            self.transfer.call((request, metadata))
        }
        async fn history(
            &self,
            account_number: &str,
            date_range: DateRange,
        ) -> Result<Vec<BankTransaction>, XandBanksErrors> {
            self.history.call((account_number.into(), date_range))
        }
    }

    #[derive(Clone)]
    struct MockHistoryFetcher {
        pub history: Mock<(Account, DateRange), Result<Vec<BankTransaction>, HistoryFetchError>>,
    }

    impl MockHistoryFetcher {
        fn new() -> Self {
            Self {
                history: Mock::new(Ok(vec![])),
            }
        }
    }

    impl HistoryFetcher for MockHistoryFetcher {
        fn get_history(
            &self,
            account: Account,
            range: DateRange,
        ) -> Result<Vec<BankTransaction>, HistoryFetchError> {
            self.history.call((account, range))
        }
    }

    #[derive(Clone)]
    struct MockBankDispatcher<'a> {
        pub get_bank_by_routing_number: Mock<String, Result<&'a dyn Bank, BankDispatcherErrors>>,
    }

    impl<'a> BankDispatcher for MockBankDispatcher<'a> {
        fn get_bank_by_routing_number(
            &self,
            routing_number: &str,
        ) -> Result<&dyn Bank, BankDispatcherErrors> {
            self.get_bank_by_routing_number.call(routing_number.into())
        }

        fn get_enriched_account(&self, _: &str) -> Result<DetailedAccount, BankDispatcherErrors> {
            unimplemented!()
        }

        fn banks(&self) -> Vec<&dyn Bank> {
            unimplemented!()
        }

        fn accounts(&self) -> Vec<DetailedAccount> {
            unimplemented!()
        }
    }

    fn create_test_config() -> TrusteeConfig {
        TrusteeConfig {
            xand_api_endpoint: Url::parse("http://fake_validator").unwrap(),
            xand_api_jwt_secret_key: None,
            xand_api_timeout_seconds: None,
            access_token_folder: "".to_string(),
            trust_address: "fake address".to_string(),
            polling_delay: 0,
            iterations_for_status_check: 0,
            redeem_retry_delay: 0,
            banks: HashSet::default(),
            partner_accounts: PartnerAccounts::default(),
            secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
                yaml_file_path: "default value is not a path".into(),
            }),
            transaction_db_path: "fake sql path".into(),
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Default::default(),
                periodic_synchronization_interval: Default::default(),
            },
        }
    }

    const TRUST_INITIAL_BALANCE: i64 = 3_000_000_000_i64;

    fn setup_mock_bank() -> MockBank {
        MockBank {
            balance: Mock::new(Ok(BankBalance {
                available_balance: Usd::from_i64_minor_units(TRUST_INITIAL_BALANCE).unwrap(),
                current_balance: Usd::from_i64_minor_units(TRUST_INITIAL_BALANCE).unwrap(),
            })),
            transfer: Mock::new(Ok(BankTransferResponse::default())),
            routing_number: Default::default(),
            reserve_account: Default::default(),
            history: Mock::new(Ok(vec![])),
        }
    }

    #[allow(non_snake_case)]
    #[test]
    fn bank_exists__bank_not_found_error_returns_false() {
        // Given
        let mock_bank_dispatcher = MockBankDispatcher {
            get_bank_by_routing_number: Mock::new(Err(BankDispatcherErrors::BankNotFound {
                routing_number: u32::MAX.to_string(),
            })),
        };
        let dispatcher = TrustDispatcher {
            history_duration: Duration::days(1),
            dispatcher: Box::new(mock_bank_dispatcher),
            cfg: Arc::new(create_test_config()),
            runtime_access: TokioAdapter::get().unwrap(),
            history_fetcher: MockHistoryFetcher::new(),
        };

        // When
        let bank_exists = dispatcher
            .bank_exists(&Account {
                account_number: "1".to_string(),
                routing_number: "arbitrary; getter is mocked".to_string(),
            })
            .unwrap();

        // Then
        assert!(!bank_exists)
    }

    #[test]
    fn test_redeem_20_000_001() {
        const BANK_ID: &str = "bank_id";
        const ACCOUNT_ID: &str = "account_id";
        const TRANSFER_AMOUNT_RUNTIME: u64 = 2_000_000_100_u64;
        const TRANSFER_AMOUNT_BANK: i64 = 2_000_000_100_i64;

        let mock_bank = setup_mock_bank();

        let dispatcher = TrustDispatcher::test_with_bank(&mock_bank);

        // Assert amount is properly handled by Usd
        mock_bank
            .transfer
            .use_fn(|(TransferRequest { amount, .. }, _)| {
                assert_eq!(
                    Usd::from_i64_minor_units(TRANSFER_AMOUNT_BANK).unwrap(),
                    amount
                );
                Ok(BankTransferResponse::default())
            });

        // execute test
        assert!(dispatcher
            .transfer(
                TRANSFER_AMOUNT_RUNTIME,
                Account::new(BANK_ID.to_string(), ACCOUNT_ID.to_string()),
                &CorrelationId::gen_random(),
            )
            .is_ok());
    }

    #[test]
    fn transfer_exists_for_create__finds_credit_txn_with_amount_9_000_000_53() {
        // Given
        const BANK_ID: &str = "bank_id";
        const ACCOUNT_ID: &str = "account_id";
        const CREATION_AMOUNT: u64 = 900_000_053_u64;
        const TRANSACTION_TYPE: BankTransactionType = BankTransactionType::Credit;
        const CORRELATION_ID: &str = "0xff7c0c8cee853ca9c02e46c25ed1126e";

        let mock_bank = setup_mock_bank();
        let dispatcher = TrustDispatcher::test_with_bank(&mock_bank);

        // configure test data
        dispatcher
            .history_fetcher
            .history
            .return_ok(vec![BankTransaction::new(
                "id".into(),
                Usd::from_u64_minor_units(CREATION_AMOUNT).unwrap(),
                CORRELATION_ID.to_string(),
                TRANSACTION_TYPE,
            )]);

        let create = PendingCreateRequest {
            amount_in_minor_unit: CREATION_AMOUNT,
            correlation_id: CorrelationId::from_str(CORRELATION_ID).unwrap(),
            account: BankAccountInfo::Unencrypted(BankAccountId {
                routing_number: BANK_ID.to_string(),
                account_number: ACCOUNT_ID.to_string(),
            }),
            completing_transaction: None,
        };

        // When
        let exists = dispatcher
            .transfer_exists_for_create(
                create,
                Account::new(BANK_ID.to_string(), ACCOUNT_ID.to_string()),
            )
            .unwrap();

        // Assert pending creation is found
        assert!(exists);
    }

    #[test]
    fn transfer_exists_for_create__does_not_use_debit_with_same_id() {
        // Given
        const BANK_ID: &str = "bank_id";
        const ACCOUNT_ID: &str = "account_id";
        const CREATION_AMOUNT: u64 = 5_u64;
        const TRANSACTION_TYPE: BankTransactionType = BankTransactionType::Debit;
        const CORRELATION_ID: &str = "0xff7c0c8cee853ca9c02e46c25ed1126e";

        let mock_bank = setup_mock_bank();
        mock_bank.history.return_ok(vec![BankTransaction::new(
            "id".into(),
            Usd::from_u64_minor_units(CREATION_AMOUNT).unwrap(),
            CORRELATION_ID.to_string(),
            TRANSACTION_TYPE,
        )]);

        let dispatcher = TrustDispatcher::test_with_bank(&mock_bank);

        let create = PendingCreateRequest {
            amount_in_minor_unit: CREATION_AMOUNT,
            correlation_id: CorrelationId::from_str(CORRELATION_ID).unwrap(),
            account: BankAccountInfo::Unencrypted(BankAccountId {
                routing_number: BANK_ID.to_string(),
                account_number: ACCOUNT_ID.to_string(),
            }),
            completing_transaction: None,
        };

        // When
        let exists = dispatcher
            .transfer_exists_for_create(
                create,
                Account::new(BANK_ID.to_string(), ACCOUNT_ID.to_string()),
            )
            .unwrap();

        // Assert pending creation WAS NOT found, because this is a "debit"
        assert!(!exists);
    }

    #[test]
    fn transfer_exists_for_redeem__finds_debit_txn() {
        // Given
        const BANK_ID: &str = "bank_id";
        const ACCOUNT_ID: &str = "account_id";
        const SMELT_AMOUNT: u64 = 100_u64;
        const TRANSACTION_TYPE: BankTransactionType = BankTransactionType::Debit;
        const CORRELATION_ID: &str = "0xff7c0c8cee853ca9c02e46c25ed1126e";

        let mock_bank = setup_mock_bank();

        mock_bank.history.return_ok(vec![BankTransaction::new(
            "id".into(),
            Usd::from_u64_minor_units(SMELT_AMOUNT).unwrap(),
            CORRELATION_ID.to_string(),
            TRANSACTION_TYPE,
        )]);

        let dispatcher = TrustDispatcher::test_with_bank(&mock_bank);

        let redeem = PendingRedeemRequest {
            amount_in_minor_unit: SMELT_AMOUNT,
            correlation_id: CorrelationId::from_str(CORRELATION_ID).unwrap(),
            account: BankAccountInfo::Unencrypted(BankAccountId {
                routing_number: BANK_ID.to_string(),
                account_number: ACCOUNT_ID.to_string(),
            }),
            completing_transaction: None,
        };

        // When
        let exists = dispatcher
            .transfer_exists_for_redeem(
                redeem,
                Account::new(BANK_ID.to_string(), ACCOUNT_ID.to_string()),
            )
            .unwrap();

        // Assert pending redeem is found
        assert!(exists);
    }

    #[test]
    fn transfer_exists_for_redeem__does_not_use_credit_txn_with_matching_id() {
        // Given
        const BANK_ID: &str = "bank_id";
        const ACCOUNT_ID: &str = "account_id";
        const SMELT_AMOUNT: u64 = 5_u64;
        const TRANSACTION_TYPE: BankTransactionType = BankTransactionType::Credit;
        const CORRELATION_ID: &str = "0xff7c0c8cee853ca9c02e46c25ed1126e";

        let mock_bank = setup_mock_bank();
        mock_bank.history.return_ok(vec![BankTransaction::new(
            "id".into(),
            Usd::from_u64_minor_units(SMELT_AMOUNT).unwrap(),
            CORRELATION_ID.to_string(),
            TRANSACTION_TYPE,
        )]);

        let dispatcher = TrustDispatcher::test_with_bank(&mock_bank);

        let redeem = PendingRedeemRequest {
            amount_in_minor_unit: SMELT_AMOUNT,
            correlation_id: CorrelationId::from_str(CORRELATION_ID).unwrap(),
            account: BankAccountInfo::Unencrypted(BankAccountId {
                routing_number: BANK_ID.to_string(),
                account_number: ACCOUNT_ID.to_string(),
            }),
            completing_transaction: None,
        };

        // When
        let exists = dispatcher
            .transfer_exists_for_redeem(
                redeem,
                Account::new(BANK_ID.to_string(), ACCOUNT_ID.to_string()),
            )
            .unwrap();

        // Assert pending redeem WAS NOT found, because this is a "credit"
        assert!(!exists);
    }

    #[tokio::test]
    async fn create_secret_store__creates_functional_local_file_store() {
        // Given
        let dir = TempDir::new().unwrap();
        let mut secrets_file = std::fs::File::create(dir.path().join("secrets.yaml")).unwrap();
        writeln!(secrets_file, "some_super_secret: value").unwrap();
        secrets_file.flush().unwrap();

        let secret_store_config = SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: dir
                .path()
                .to_path_buf()
                .join("secrets.yaml")
                .to_string_lossy()
                .to_string(),
        });

        // When
        let secret_store = create_secret_store(&secret_store_config);

        // Then
        secret_store.check_health().await.unwrap();
        assert_eq!(
            secret_store
                .read("some_super_secret")
                .await
                .unwrap()
                .expose_secret(),
            "value"
        );
    }
}
