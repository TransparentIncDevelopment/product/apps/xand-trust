use crate::test_utils::test_val::TestVal;

impl TestVal for xand_api_client::CorrelationId {
    fn test_val() -> Self {
        let value: [u8; 16] = [1; 16];
        value.into()
    }
}
