use crate::test_utils::test_val::TestVal;

impl TestVal for xand_api_client::PendingCreateRequest {
    fn test_val() -> Self {
        Self {
            amount_in_minor_unit: 0,
            correlation_id: TestVal::test_val(),
            account: TestVal::test_val(),
            completing_transaction: None,
        }
    }
}
