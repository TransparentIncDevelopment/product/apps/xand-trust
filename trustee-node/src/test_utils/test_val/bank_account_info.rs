use crate::test_utils::test_val::TestVal;
use xand_api_client::BankAccountId;

impl TestVal for xand_api_client::BankAccountInfo {
    fn test_val() -> Self {
        Self::Unencrypted(BankAccountId {
            routing_number: "000".to_string(),
            account_number: "111".to_string(),
        })
    }
}
