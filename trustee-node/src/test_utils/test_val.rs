//! This module contains implementations of `TestVal` for various external types.
//! These default values are consumed in various test cases.

pub mod bank_account_info;
pub mod correlation_id;
pub mod pending_create;

/// This trait is used internally for testing, providing default test values for third party types
/// that don't implement `Default`
pub trait TestVal {
    fn test_val() -> Self;
}
