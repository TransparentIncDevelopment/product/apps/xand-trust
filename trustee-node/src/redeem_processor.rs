use crate::{
    core::{BankError, RedeemProcessor, TrustBankDispatcher, TxnIssuer},
    logging_events::LoggingEvent,
    TrustError,
};
#[cfg(test)]
mod tests;
#[cfg(test)]
use fake_clock::FakeClock as Instant;
#[cfg(not(test))]
use std::time::Instant;
use std::{
    collections::{HashMap, HashSet},
    time::Duration,
};
use trustee_node_config::Account;
use xand_api_client::{
    BankAccountInfo, CorrelationId, PendingRedeemRequest, RedeemCancellationReason,
};

#[derive(PartialEq, Debug, Clone)]
enum RedeemStatus {
    New,
    TransferCompleted,
    Completed,
    CancelMe { reason: RedeemCancellationReason },
    Retry { last_attempt: Instant },
}

#[derive(PartialEq, Debug)]
struct StatusEntry {
    redeem: PendingRedeemRequest,
    status: RedeemStatus,
}

pub struct InMemoryRedeemProcessor<'a, BD: TrustBankDispatcher, T: TxnIssuer> {
    //Maps correlation id to status of a redeem
    pending_redeems: HashMap<CorrelationId, StatusEntry>,
    dispatcher: &'a BD,
    txn_issuer: &'a T,
    redeem_retry_delay: Duration,
}

impl<'a, BD: TrustBankDispatcher, T: TxnIssuer> RedeemProcessor
    for InMemoryRedeemProcessor<'a, BD, T>
{
    #[tracing::instrument(skip_all)]
    fn receive_unfulfilled_redeems(&mut self, unfulfilled_redeems: Vec<PendingRedeemRequest>) {
        tracing::info!(message = ?LoggingEvent::ReceivedUnfulfilledRedeems {
            num_redeems: unfulfilled_redeems.len()
        });
        self.reconcile_new_pending_redemptions(unfulfilled_redeems);
        for entry in self.pending_redeems.values_mut() {
            tracing::info!(message = ?LoggingEvent::ProcessingPendingRedeem {
                id: entry.redeem.correlation_id.clone()
            });
            match &entry.redeem.account {
                BankAccountInfo::Unencrypted(id) => {
                    let account =
                        Account::new(id.account_number.clone(), id.routing_number.clone());
                    // checks if the redeem is in a retry state and enough time has elapsed
                    let can_retry = if let RedeemStatus::Retry { last_attempt } = entry.status {
                        last_attempt.elapsed() >= self.redeem_retry_delay
                    } else {
                        false
                    };
                    if entry.status == RedeemStatus::New || can_retry {
                        attempt_fulfillment(self.dispatcher, entry, account);
                    }
                }
                BankAccountInfo::Encrypted(_) => match entry.status {
                    RedeemStatus::New => {
                        entry.status = RedeemStatus::CancelMe {
                            reason: RedeemCancellationReason::InvalidData,
                        };
                        tracing::warn!(message = ?LoggingEvent::PendingRedeemRequiresCancellation {
                            redeem: entry.redeem.clone(),
                            message:
                                "Incorrectly formatted redemption request! Bank information could not be read."
                                    .to_string()
                        });
                    }
                    RedeemStatus::Retry { .. } => {
                        tracing::error!(message = ?LoggingEvent::TrustError {
                            err: TrustError::LogicError {message: "Incorrectly formatted redemption is unexpectedly in retry state.  Bank information could not be read.".to_string()}
                        });
                    }
                    _ => {}
                },
            };

            if entry.status == RedeemStatus::TransferCompleted {
                tracing::info!(message = ?LoggingEvent::IssuingFulfillmentForRedeem {
                    id: entry.redeem.correlation_id.clone()
                });
                match self.txn_issuer.record_fulfilled_redeem(&entry.redeem) {
                    Ok(()) => {
                        let event = LoggingEvent::FulfillmentIssuedForRedeem {
                            id: entry.redeem.correlation_id.clone(),
                        };
                        tracing::info!(message = ?event);
                        entry.status = RedeemStatus::Completed;
                    }
                    Err(e) => {
                        tracing::error!(message = ?LoggingEvent::ErrorIssuingFulfillmentForRedeem {
                            redeem: entry.redeem.clone(),
                            err: e.clone()
                        })
                    }
                }
            }
            if let RedeemStatus::CancelMe { reason } = &entry.status {
                tracing::info!(message = ?LoggingEvent::IssuingCancellationForRedeem {
                    id: entry.redeem.correlation_id.clone(),
                    reason: reason.to_string()
                });
                match self.txn_issuer.cancel_redeem(&entry.redeem, reason.clone()) {
                    Ok(()) => {
                        let event = LoggingEvent::CancellationIssuedForRedeem {
                            id: entry.redeem.correlation_id.clone(),
                            reason: reason.to_string(),
                        };
                        tracing::info!(message = ?event);
                        entry.status = RedeemStatus::Completed
                    }
                    Err(e) => {
                        let event = LoggingEvent::ErrorDuringCancellationForRedeem {
                            redeem: entry.redeem.clone(),
                            err: e,
                        };
                        tracing::error!(message = ?event)
                    }
                }
            }
        }
    }
}

impl<'a, BD: TrustBankDispatcher, T: TxnIssuer> InMemoryRedeemProcessor<'a, BD, T> {
    pub fn new(
        bank_dispatcher: &'a BD,
        txn_issuer: &'a T,
        redeem_retry_delay: Duration,
    ) -> InMemoryRedeemProcessor<'a, BD, T> {
        InMemoryRedeemProcessor {
            pending_redeems: HashMap::new(),
            dispatcher: bank_dispatcher,
            txn_issuer,
            redeem_retry_delay,
        }
    }

    /// Every time we query for pending redemptions, it is expected that that list represents
    /// the current legitimately outstanding redemptions. As a result, ones we complete will be
    /// missing in later calls - and sometimes ones the trust does *not* complete may also be
    /// missing, because they were cancelled by the user. Those also need to be removed from our
    /// pending map so that we do not try to fulfill them.
    #[tracing::instrument(skip_all)]
    fn reconcile_new_pending_redemptions(
        &mut self,
        redemption_requests: Vec<PendingRedeemRequest>,
    ) {
        // Go through all the redemptions we *do* know about and eliminate ones that the
        // chain no longer thinks exist if they are completed.
        let old_pending_id_list = self
            .pending_redeems
            .keys()
            .cloned()
            .collect::<HashSet<CorrelationId>>();
        self.pending_redeems.retain(|id, redemption| {
            redemption.status != RedeemStatus::Completed
                || redemption_requests
                    .iter()
                    .any(|pr| pr.correlation_id == *id)
        });
        let removed_pending_id_list = &old_pending_id_list
            - &self
                .pending_redeems
                .keys()
                .cloned()
                .collect::<HashSet<CorrelationId>>();
        if !removed_pending_id_list.is_empty() {
            tracing::info!(message = ?LoggingEvent::RemovedDuplicatePendingRedeems {
                removed_id_list: removed_pending_id_list.iter().cloned().collect::<Vec<CorrelationId>>()
            });
        }

        // Add all new redemptions we haven't seen before
        for redemption in redemption_requests {
            if !self
                .pending_redeems
                .contains_key(&redemption.correlation_id)
            {
                self.pending_redeems.insert(
                    redemption.correlation_id.clone(),
                    StatusEntry {
                        redeem: redemption.clone(),
                        status: RedeemStatus::New,
                    },
                );
                tracing::info!(message = ?LoggingEvent::AddedNewPendingRedeem {
                    id: redemption.correlation_id.clone()
                });
            }
        }
    }
}
#[tracing::instrument(skip(dispatcher))]
fn attempt_fulfillment<BD>(dispatcher: &BD, entry: &mut StatusEntry, account: Account)
where
    BD: TrustBankDispatcher,
{
    // Ensure the account that is being redeemed into is an allowlisted account
    if !dispatcher.is_allowlisted(&account) {
        let event = LoggingEvent::AccountIsNotAllowlisted { account };
        tracing::error!(message = ?event);
        entry.status = RedeemStatus::CancelMe {
            reason: RedeemCancellationReason::AccountNotAllowed,
        };
        return;
    }
    // Verify that the transaction hasn't already been performed:

    match dispatcher.transfer_exists_for_redeem(entry.redeem.clone(), account.clone()) {
        Ok(txn_exists) => {
            if !txn_exists {
                tracing::info!(message = ?LoggingEvent::InitiatingBankTransferForRedeem {
                    id: entry.redeem.correlation_id.clone()
                });
                let transfer_result = dispatcher.transfer(
                    entry.redeem.amount_in_minor_unit,
                    account.clone(),
                    &entry.redeem.correlation_id,
                );
                match transfer_result {
                    Ok(_) => {
                        tracing::info!(message = ?LoggingEvent::BankTransferCompletedForRedeem {
                            id: entry.redeem.correlation_id.clone()
                        });
                        entry.status = RedeemStatus::TransferCompleted;
                    }
                    Err(BankError::InsufficientBalance) => {
                        let event = LoggingEvent::ReserveBalanceInsufficient {
                            routing_number: account.routing_number,
                        };
                        tracing::error!(message = ?event);
                        entry.status = RedeemStatus::Retry {
                            last_attempt: Instant::now(),
                        }
                    }
                    Err(e) => {
                        let event = LoggingEvent::BankTransferFailedForRedeem {
                            id: entry.redeem.correlation_id.clone(),
                            reason: e,
                        };
                        tracing::error!(message = ?event);
                        // put into a retry state with a timeout
                        entry.status = RedeemStatus::Retry {
                            last_attempt: Instant::now(),
                        }
                    }
                }
            } else {
                let event = LoggingEvent::BankTransferFoundForRedeem {
                    id: entry.redeem.correlation_id.clone(),
                };
                tracing::warn!(message = ?event);
                entry.status = RedeemStatus::TransferCompleted;
            }
        }
        Err(e) => {
            let event = LoggingEvent::ErrorCheckingTxnForRedeem {
                redeem: entry.redeem.clone(),
                reason: e,
            };
            tracing::error!(message = ?event)
        }
    }
}
