//! This is copied from an older version of xand-banks to minimize churn while upgrading to it's
//! newest version. It should be re-thought when support for not-file-based secrets is added

use snafu::OptionExt;
use std::{borrow::Borrow, collections::HashMap, sync::Arc};
use xand_banks::{errors::XandBanksErrors, models::account::DetailedAccount, Bank, BanksConfig};
use xand_secrets::SecretKeyValueStore;

/// Convenience method that creates banks from a config and uses that to instantiate a
/// XandBankDispatcher.
pub fn create_dispatcher<T: BanksConfig>(
    banks: &T,
    secret_store: Arc<dyn SecretKeyValueStore>,
) -> Result<XandBankDispatcher> {
    Ok(XandBankDispatcher::new(banks.get_banks(secret_store)?))
}

/// Bank dispatchers implement this trait to facilitate ease of mocking in tests.
pub trait BankDispatcher {
    /// Given a bank id (routing number, name, etc) return a reference to the client
    /// in charge of servicing requests to that bank.
    /// If a matching bank client is not available, returns an Error::BankName.
    fn get_bank_by_routing_number(&self, routing_number: &str) -> Result<&dyn Bank>;

    fn get_enriched_account(&self, account_id: &str) -> Result<DetailedAccount>;

    fn banks(&self) -> Vec<&dyn Bank>;

    fn accounts(&self) -> Vec<DetailedAccount>;
}

#[derive(Debug, Snafu, Clone, Serialize)]
#[snafu(visibility(pub(crate)))]
pub enum BankDispatcherErrors {
    #[snafu(display("No bank found matching {}", routing_number))]
    BankNotFound { routing_number: String },
    #[snafu(display("No account found matching {}", account_id))]
    AccountNotFound { account_id: String },
    #[snafu(display("Could not enrich account: {}", source))]
    #[snafu(context(false))]
    CouldNotEnrichAccount { source: XandBanksErrors },
}

type Result<T, E = BankDispatcherErrors> = std::result::Result<T, E>;

#[derive(Clone)]
pub struct XandBankDispatcher {
    // a mapping of accounts to their bank ID for easy lookup
    pub account_to_bank_id: HashMap<String, String>,
    pub banks: HashMap<String, Box<dyn Bank>>,
}

impl XandBankDispatcher {
    pub fn new(banks: HashMap<String, Box<dyn Bank>>) -> Self {
        let mut accounts = HashMap::new();
        for (bank_id, bank) in &banks {
            for account in bank.accounts() {
                accounts.insert(account.id.clone(), bank_id.clone());
            }
        }
        Self {
            account_to_bank_id: accounts,
            banks,
        }
    }

    fn get_bank_by_account_id(&self, account_id: &str) -> Result<&dyn Bank> {
        let account = self
            .account_to_bank_id
            .get(account_id)
            .context(AccountNotFound {
                account_id: account_id.to_string(),
            })?;

        let bank = self.banks.get(account).context(BankNotFound {
            routing_number: account.to_string(),
        })?;

        Ok(bank.borrow())
    }
}

impl BankDispatcher for XandBankDispatcher {
    fn get_bank_by_routing_number(&self, routing_number: &str) -> Result<&dyn Bank> {
        let bank = self.banks.get(routing_number).context(BankNotFound {
            routing_number: routing_number.to_string(),
        })?;

        Ok(bank.borrow())
    }

    fn get_enriched_account(&self, account_id: &str) -> Result<DetailedAccount> {
        Ok(self
            .get_bank_by_account_id(account_id)?
            .get_enriched_account(account_id)?)
    }

    fn banks(&self) -> Vec<&dyn Bank> {
        let mut borrowables = vec![];
        for bank in self.banks.values() {
            borrowables.push(bank.as_ref())
        }
        borrowables
    }

    fn accounts(&self) -> Vec<DetailedAccount> {
        let mut borrowables = vec![];
        for bank in self.banks.values() {
            for account in bank.accounts() {
                borrowables.push(account)
            }
        }
        borrowables
    }
}

#[cfg(test)]
mod dispatcher_tests {
    use super::*;
    use pseudo::Mock;
    use xand_banks::{
        banks::XandBank,
        date_range::DateRange,
        models::{
            account::{Account, TransferRequest},
            BankBalance, BankTransaction, BankTransferResponse,
        },
        BankAdapter,
    };

    const NOOK_DISPLAY: &str = "Nook National Bank";
    const NOOK_ROUTING: &str = "111222333";

    const RACCOON_DISPLAY: &str = "Raccoon City Savings and Loans";
    const RACCOON_ROUTING: &str = "333222111";

    #[derive(Clone, Debug)]
    pub struct MockBankAdapter {
        pub balance: Mock<String, Result<BankBalance, XandBanksErrors>>,
        pub transfer:
            Mock<(TransferRequest, Option<String>), Result<BankTransferResponse, XandBanksErrors>>,
        pub history: Mock<(String, DateRange), Result<Vec<BankTransaction>, XandBanksErrors>>,
    }

    impl MockBankAdapter {
        pub fn new(
            balance_res: Result<BankBalance, XandBanksErrors>,
            transfer_res: Result<BankTransferResponse, XandBanksErrors>,
            history_res: Result<Vec<BankTransaction>, XandBanksErrors>,
        ) -> Self {
            Self {
                balance: Mock::new(balance_res),
                transfer: Mock::new(transfer_res),
                history: Mock::new(history_res),
            }
        }

        pub fn boxed(&self) -> Box<Self> {
            Box::new(self.clone())
        }
    }

    impl Default for MockBankAdapter {
        fn default() -> Self {
            Self::new(
                Ok(BankBalance::default()),
                Ok(BankTransferResponse::default()),
                Ok(vec![]),
            )
        }
    }

    #[async_trait::async_trait]
    impl BankAdapter for MockBankAdapter {
        async fn balance(&self, account_id: &str) -> Result<BankBalance, XandBanksErrors> {
            self.balance.call(account_id.to_string())
        }

        async fn transfer(
            &self,
            request: TransferRequest,
            metadata: Option<String>,
        ) -> Result<BankTransferResponse, XandBanksErrors> {
            self.transfer.call((request, metadata))
        }

        async fn history(
            &self,
            account_id: &str,
            date_range: DateRange,
        ) -> Result<Vec<BankTransaction>, XandBanksErrors> {
            self.history.call((account_id.to_string(), date_range))
        }

        fn memo_char_limit(&self) -> u32 {
            42
        }
    }

    fn bank_dispatcher_builder() -> XandBankDispatcher {
        let banks = animal_bank_map_builder();

        XandBankDispatcher::new(banks)
    }

    fn animal_bank_map_builder() -> HashMap<String, Box<dyn Bank>> {
        let mut nook_bank = XandBank {
            name: NOOK_DISPLAY.to_string(),
            adapter: MockBankAdapter::default().boxed(),
            routing_number: NOOK_ROUTING.to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        nook_bank.accounts.insert(
            "tom_nook".to_string(),
            Account {
                id: "tom_nook".to_string(),
                short_name: "Tom Nook".to_string(),
                account_number: "7185657367".to_string(),
            },
        );
        let mut raccoon_bank = XandBank {
            name: RACCOON_DISPLAY.to_string(),
            adapter: MockBankAdapter::default().boxed(),
            routing_number: RACCOON_ROUTING.to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        raccoon_bank.accounts.insert(
            "timmy".to_string(),
            Account {
                id: "timmy".to_string(),
                short_name: "Timmy the Raccoon".to_string(),
                account_number: "6314237641".to_string(),
            },
        );
        raccoon_bank.accounts.insert(
            "tommy".to_string(),
            Account {
                id: "tommy".to_string(),
                short_name: "Tommy the Raccoon".to_string(),
                account_number: "2122202923".to_string(),
            },
        );
        let mut banks: HashMap<String, Box<dyn Bank>> = HashMap::new();
        banks.insert("nook_bank".to_string(), Box::new(nook_bank));
        banks.insert("raccoon_bank".to_string(), Box::new(raccoon_bank));
        banks
    }

    #[test]
    fn correctly_builds_accounts_map() {
        let dispatcher = bank_dispatcher_builder();

        assert_eq!(
            dispatcher.account_to_bank_id.get("tom_nook").unwrap(),
            "nook_bank"
        );
        assert_eq!(
            dispatcher.account_to_bank_id.get("timmy").unwrap(),
            "raccoon_bank"
        );
        assert_eq!(
            dispatcher.account_to_bank_id.get("tommy").unwrap(),
            "raccoon_bank"
        );
    }

    #[test]
    fn correctly_routes_account() {
        let dispatcher = bank_dispatcher_builder();

        let tom_nooks_bank_name = dispatcher
            .get_bank_by_account_id("tom_nook")
            .unwrap()
            .display_name();
        let timmy_bank_name = dispatcher
            .get_bank_by_account_id("timmy")
            .unwrap()
            .display_name();
        let tommy_bank_name = dispatcher
            .get_bank_by_account_id("tommy")
            .unwrap()
            .display_name();

        assert_eq!(tom_nooks_bank_name, NOOK_DISPLAY);
        assert_eq!(timmy_bank_name, RACCOON_DISPLAY);
        assert_eq!(tommy_bank_name, RACCOON_DISPLAY);
    }

    #[test]
    fn correctly_routes_by_routing_number() {
        let dispatcher = bank_dispatcher_builder();

        let nook_bank_name = dispatcher
            .get_bank_by_routing_number("nook_bank")
            .unwrap()
            .display_name();
        assert_eq!(nook_bank_name, NOOK_DISPLAY);

        let nook_bank_name = dispatcher
            .get_bank_by_routing_number("raccoon_bank")
            .unwrap()
            .display_name();
        assert_eq!(nook_bank_name, RACCOON_DISPLAY);
    }

    #[test]
    fn unknown_bank_returns_err() {
        let dispatcher = bank_dispatcher_builder();
        let res = dispatcher.get_bank_by_account_id("batman");

        match res.err().unwrap() {
            BankDispatcherErrors::AccountNotFound { account_id } => {
                assert_eq!(account_id, "batman")
            }
            _ => panic!("Got unexpected error!"),
        }
    }

    #[test]
    fn unknown_account_returns_err() {
        let dispatcher = bank_dispatcher_builder();
        let res = dispatcher.get_bank_by_routing_number("gotham bank");

        match res.err().unwrap() {
            BankDispatcherErrors::BankNotFound { routing_number } => {
                assert_eq!(routing_number, "gotham bank")
            }
            _ => panic!("Got unexpected error!"),
        }
    }
}
