use error::Result;
use opentelemetry_json::exporter::json;
use opentelemetry_json::pipeline;
use tracing::level_filters::LevelFilter;
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt, Layer};

pub mod error;

const TRACE_LOGS_SUB_FOLDER: &str = "traces";
const TRACE_LOG_FILENAME_PREFIX: &str = "tracedata.log";
const OPEN_TELEMETRY_LOG_FILENAME_PREFIX: &str = "opentelemetry.log";

pub fn try_init() -> Result<TracingGuard> {
    let subscriber = tracing_subscriber::Registry::default();

    // Set up trace data folder and filename
    let trace_data_dir = std::env::current_dir()?.join(TRACE_LOGS_SUB_FOLDER);
    std::fs::create_dir_all(&trace_data_dir)?;
    println!("Writing trace data to: {:?}", trace_data_dir);

    // Build open telemetry json log layer
    let open_telemetry_trace_file_prefix = trace_data_dir.join(OPEN_TELEMETRY_LOG_FILENAME_PREFIX);
    let open_telemetry_rolling_appender = rolling_file::BasicRollingFileAppender::new(
        open_telemetry_trace_file_prefix,
        rolling_file::RollingConditionBasic::new().daily(),
        7,
    )?;
    let open_telemetry_json_exporter = json::Exporter::new(open_telemetry_rolling_appender);
    let tracer = pipeline::Builder::default()
        .add_simple_exporter(open_telemetry_json_exporter)
        .install();
    let open_telemetry_layer = tracing_opentelemetry::layer()
        .with_tracer(tracer)
        .with_filter(LevelFilter::DEBUG);

    // Build human readable tracing log layer
    let trace_file_prefix = trace_data_dir.join(TRACE_LOG_FILENAME_PREFIX);
    let trace_rolling_appender = rolling_file::BasicRollingFileAppender::new(
        trace_file_prefix,
        rolling_file::RollingConditionBasic::new().daily(),
        7,
    )?;
    let (non_blocking_trace_appender, worker_guard) =
        tracing_appender::non_blocking(trace_rolling_appender);
    let trace_file_layer = tracing_subscriber::fmt::layer()
        .with_ansi(false)
        .with_writer(non_blocking_trace_appender)
        .with_filter(LevelFilter::INFO);

    // Set up stdout layer
    let stdout_writer = std::io::stdout;
    let stdout_formatted_layer = tracing_subscriber::fmt::layer()
        .with_writer(stdout_writer)
        .with_filter(LevelFilter::INFO);

    subscriber
        .with(trace_file_layer)
        .with(open_telemetry_layer)
        .with(stdout_formatted_layer)
        .try_init()?;

    Ok(TracingGuard::new(worker_guard))
}

/// Drop this guard when trace logging is done (i.e. when trust terminates), to cleanup tracing and
/// flush log file buffers.
pub struct TracingGuard {
    #[allow(dead_code)]
    worker_guard: WorkerGuard,
}

impl TracingGuard {
    pub fn new(worker_guard: WorkerGuard) -> Self {
        Self { worker_guard }
    }
}
