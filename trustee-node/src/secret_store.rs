use std::sync::Arc;
use trustee_node_config::SecretStoreConfig;
use xand_secrets::SecretKeyValueStore;
use xand_secrets_local_file::LocalFileSecretKeyValueStore;

pub fn create_secret_store(
    secret_store_config: &SecretStoreConfig,
) -> Arc<dyn SecretKeyValueStore> {
    match secret_store_config {
        SecretStoreConfig::LocalFile(file_config) => Arc::new(
            LocalFileSecretKeyValueStore::create_from_config(file_config.clone()),
        ),
    }
}
