use crate::async_adapter::ports::future_resolver::FutureResolver;
use std::future::Future;

/// Extension trait to simplify driving a dependencies' Futures to completion in synchronous contexts.
/// It's intended to mirror the postfix style of `.await` to leave the synchronous code more readable
/// and keep async dependencies chainable
pub trait SyncFutureExt: Future {
    fn block_with<B: FutureResolver>(self, rt: &B) -> Self::Output;
}

impl<T> SyncFutureExt for T
where
    T: Future + Sized,
{
    fn block_with<B: FutureResolver>(self, rt: &B) -> Self::Output {
        rt.block(self)
    }
}
