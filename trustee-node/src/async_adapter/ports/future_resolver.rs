use std::future::Future;

/// Trait through which services or adapters within the trust application can synchronously drive external
/// futures to completion
pub trait FutureResolver {
    /// Blocks current thread; Resolves future
    fn block<F: Future>(&self, fut: F) -> F::Output;
}
