#![allow(non_snake_case)]
use super::*;

/// Test in a default, sync context by not using the #[tokio::test] annotation
#[test]
fn get__returns_ok_in_synchronous_context() {
    // Then
    let _t = TokioAdapter::get().unwrap();
}

/// Test invoking within a tokio runtime by using the #[tokio::test] annotation
#[tokio::test]
async fn get__returns_ok_in_asynchronous_runtime() {
    // Then
    let _t = TokioAdapter::get().unwrap();
}

#[test]
fn block__can_resolve_future() {
    // Given
    async fn async_fn() -> u32 {
        123
    }
    let t = TokioAdapter::get().unwrap();

    // When
    let res = t.block(async_fn());

    // Then
    assert_eq!(res, 123);
}
