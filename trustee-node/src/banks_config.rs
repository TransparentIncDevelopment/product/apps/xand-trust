use std::collections::HashSet;
use std::{collections::HashMap, sync::Arc};

use trustee_node_config::trust_banks::TrustBanksConfig;
use xand_banks::{
    banks::{config::BankConfig, XandBank},
    errors::XandBanksErrors,
    models::account::Account,
    Bank, BanksConfig,
};
use xand_secrets::SecretKeyValueStore;

pub fn create_bank_from_bank_config(
    c: BankConfig,
    secret_store: Arc<dyn SecretKeyValueStore>,
) -> Result<XandBank, XandBanksErrors> {
    let adapter = c.create_adapter(secret_store)?;
    Ok(XandBank {
        name: c.name.clone(),
        adapter,
        routing_number: c.routing_number.clone(),
        trust_account: Account {
            id: "trust".to_owned(),
            short_name: c.name.clone(),
            account_number: c.trust_account.clone(),
        },
        accounts: HashMap::new(),
    })
}

pub struct TrustBanksCreator {
    pub configs: HashSet<TrustBanksConfig>,
}

impl BanksConfig for TrustBanksCreator {
    fn get_banks(
        &self,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<HashMap<String, Box<dyn Bank>>, XandBanksErrors> {
        let mut map = HashMap::new();

        // Helper function to create Banks from BankConfigs
        fn reg_bank(
            c: BankConfig,
            map: &mut HashMap<String, Box<dyn Bank>>,
            ss: Arc<dyn SecretKeyValueStore>,
        ) -> Result<(), XandBanksErrors> {
            map.insert(
                c.routing_number.to_string(),
                Box::new(create_bank_from_bank_config(c, ss)?) as Box<dyn Bank>,
            );
            Ok(())
        }

        for cfg in &self.configs {
            match cfg {
                TrustBanksConfig::Mcb(c) => {
                    reg_bank(c.clone().into(), &mut map, secret_store.clone())?
                }
                TrustBanksConfig::TreasuryPrime(c) => {
                    reg_bank(c.clone().into(), &mut map, secret_store.clone())?
                }
            }
        }

        Ok(map)
    }
}
