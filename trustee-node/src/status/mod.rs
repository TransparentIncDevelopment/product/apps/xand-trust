use crate::{
    core::{CreateProcessor, RedeemProcessor, ServicesChecker, TrustBankDispatcher},
    errors::HealthCheckErrorReport,
    logging_events::LoggingEvent,
    metrics::record_bank_balance,
    poller::Poller,
    Result,
};

#[derive(Constructor)]
pub struct CheckServices<'a, BD: TrustBankDispatcher, C: CreateProcessor, R: RedeemProcessor> {
    poller: Poller<C, R>,
    bank_dispatcher: &'a BD,
}

impl<'a, BD: TrustBankDispatcher, C: CreateProcessor, R: RedeemProcessor>
    CheckServices<'a, BD, C, R>
{
    pub fn poller(&mut self) -> &mut Poller<C, R> {
        &mut self.poller
    }
}

impl<'a, BD: TrustBankDispatcher, C: CreateProcessor, R: RedeemProcessor> ServicesChecker
    for CheckServices<'a, BD, C, R>
{
    /// Checks service health, as well as records high-level metrics those services provides
    /// (presuming they are healthy and available)
    #[tracing::instrument(skip(self), err)]
    fn check_services(&self) -> Result<(), HealthCheckErrorReport> {
        let poller_health_check_res = self.poller.health_check();
        match poller_health_check_res {
            Ok(_) => {
                tracing::info!(message = ?LoggingEvent::XandApiHealthCheckPassed);
            }
            Err(_) => {
                tracing::error!(message = ?LoggingEvent::XandApiHealthCheckFailed)
            }
        }

        let mut reserve_balances_error = vec![];
        self.bank_dispatcher
            .reserve_balances()
            .into_iter()
            .for_each(|(routing_number, bal_res)| match bal_res {
                Ok(bal) => {
                    record_bank_balance(self.poller.metrics(), &routing_number, &bal);
                    let event = LoggingEvent::BankHealthCheckPassed { routing_number };
                    tracing::info!(message = ?event);
                }
                Err(err) => {
                    reserve_balances_error.push(err.into());
                    let event = LoggingEvent::BankHealthCheckFailed { routing_number };
                    tracing::error!(message = ?event);
                }
            });

        let errors: Vec<_> = poller_health_check_res
            .err()
            .into_iter()
            .chain(reserve_balances_error.into_iter())
            .collect();

        if errors.is_empty() {
            Ok(())
        } else {
            Err(errors.into())
        }
    }
}

#[cfg(test)]
mod test {
    #![allow(non_snake_case)]
    use std::sync::Arc;

    use super::*;
    use crate::{
        core::BankError,
        metrics::{AVAILABLE_BALANCE_DIM, CURRENT_BALANCE_DIM},
        poller::Poller,
        test_utils::{
            test_val::TestVal, ConfigurableMockXandApiClient, FakeCreateProcessor,
            FakeRedeemProcessor, MockTrustBankDispatcher,
        },
        TrustError,
    };
    use insta::assert_display_snapshot;
    use pseudo::Mock;
    use std::collections::HashMap;
    use tokio::runtime::Runtime;
    use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
    use tracing_assert_macros::tracing_capture_event_fields;
    use xand_api_client::{
        errors::XandApiClientError, mock::MockXandApiClient, Blockstamp, TotalIssuance,
    };
    use xand_banks::{
        models::BankBalance,
        xand_money::{Money, Usd},
    };

    fn configure_dispatcher_succeeding(dispatcher: &MockTrustBankDispatcher) {
        dispatcher
            .mock_all_reserve_balances
            .return_value(HashMap::new());
    }

    fn configure_dispatcher_erroring(dispatcher: &MockTrustBankDispatcher) {
        let mut balmap = HashMap::new();
        balmap.insert("1234".to_owned(), Err(BankError::UnknownError));
        dispatcher.mock_all_reserve_balances.return_value(balmap);
    }

    fn configure_xand_api_client_succeeding(client: &ConfigurableMockXandApiClient) {
        let valid_pending_create = xand_api_client::PendingCreateRequest {
            amount_in_minor_unit: 1,
            account: xand_api_client::BankAccountInfo::Unencrypted(
                xand_api_client::BankAccountId {
                    routing_number: "bankid".to_string(),
                    account_number: "accountid".to_string(),
                },
            ),
            correlation_id: xand_api_client::CorrelationId::test_val(),
            completing_transaction: None,
        };
        client
            .get_pending_creation_requests
            .return_ok::<xand_api_client::models::Paginated<Vec<_>>>(
                vec![valid_pending_create].into(),
            );
        client.get_total_issuance.return_ok(TotalIssuance {
            total_issued: 1,
            blockstamp: Blockstamp {
                block_number: 10,
                unix_timestamp_ms: 1667949150,
                is_stale: false,
            },
        });
    }

    fn configure_xand_api_client_erroring(client: &ConfigurableMockXandApiClient) {
        client
            .get_pending_creation_requests
            .return_err(XandApiClientError::BadRequest {
                message: "fake xand-api client error".into(),
            });
        client
            .get_total_issuance
            .return_err(XandApiClientError::BadRequest {
                message: "fake xand-api client error".into(),
            });
    }

    #[allow(clippy::float_cmp)]
    #[test]
    fn records_balance_metrics() {
        let mock_client = Box::<MockXandApiClient>::default();
        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let poller = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );
        let dispatcher = MockTrustBankDispatcher::default();
        let mut balmap = HashMap::new();
        balmap.insert(
            "1234".to_owned(),
            Ok(BankBalance {
                available_balance: Usd::from_i64_minor_units(1000).unwrap(),
                current_balance: Usd::from_i64_minor_units(2000).unwrap(),
            }),
        );
        balmap.insert(
            "5678".to_owned(),
            Ok(BankBalance {
                available_balance: Usd::from_i64_minor_units(3000).unwrap(),
                current_balance: Usd::from_i64_minor_units(4000).unwrap(),
            }),
        );
        dispatcher.mock_all_reserve_balances.return_value(balmap);

        let checker = CheckServices::new(poller, &dispatcher);
        checker.check_services().unwrap();

        // Verify that the bank balance metrics were recorded
        assert_eq!(
            checker
                .poller
                .metrics()
                .bank_balances
                .with_dims(vec!["1234", AVAILABLE_BALANCE_DIM])
                .unwrap()
                .get(),
            10.0
        );
        assert_eq!(
            checker
                .poller
                .metrics()
                .bank_balances
                .with_dims(vec!["1234", CURRENT_BALANCE_DIM])
                .unwrap()
                .get(),
            20.0
        );
        assert_eq!(
            checker
                .poller
                .metrics()
                .bank_balances
                .with_dims(vec!["5678", AVAILABLE_BALANCE_DIM])
                .unwrap()
                .get(),
            30.0
        );
        assert_eq!(
            checker
                .poller
                .metrics()
                .bank_balances
                .with_dims(vec!["5678", CURRENT_BALANCE_DIM])
                .unwrap()
                .get(),
            40.0
        );
    }

    #[test]
    fn health_check__identifies_poller_errors() {
        let mock_client = Box::<ConfigurableMockXandApiClient>::default();
        configure_xand_api_client_erroring(&mock_client);

        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let poller = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );
        let dispatcher = MockTrustBankDispatcher::default();
        configure_dispatcher_succeeding(&dispatcher);

        let checker = CheckServices::new(poller, &dispatcher);

        let error = checker.check_services().unwrap_err();
        assert_display_snapshot!(error, @r###"["Underlying client error (Error while fetching total claims during health check): Bad request: fake xand-api client error"]"###);
    }

    #[test]
    fn health_check__identifies_dispatcher_errors() {
        let mock_client = Box::<ConfigurableMockXandApiClient>::default();
        configure_xand_api_client_succeeding(&mock_client);

        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let poller = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );
        let dispatcher = MockTrustBankDispatcher::default();
        configure_dispatcher_erroring(&dispatcher);

        let checker = CheckServices::new(poller, &dispatcher);

        let error = checker.check_services().unwrap_err();
        assert_display_snapshot!(error, @r###"["Error with bank: UnknownError"]"###);
    }

    #[test]
    fn health_check__identifies_simultaneous_dispatcher_and_poller_errors() {
        let mock_client = Box::<ConfigurableMockXandApiClient>::default();
        configure_xand_api_client_erroring(&mock_client);

        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let poller = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );
        let dispatcher = MockTrustBankDispatcher::default();
        configure_dispatcher_erroring(&dispatcher);

        let checker = CheckServices::new(poller, &dispatcher);

        let error = checker.check_services().unwrap_err();
        assert_display_snapshot!(error, @r###"["Underlying client error (Error while fetching total claims during health check): Bad request: fake xand-api client error", "Error with bank: UnknownError"]"###);
    }

    #[test]
    fn health_check__emits_health_check_passed_events() {
        // Given
        let mock_client = Box::<ConfigurableMockXandApiClient>::default();
        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let poller = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );

        let mut balances = HashMap::new();
        balances.insert("this_routing_number".into(), Ok(BankBalance::default()));
        balances.insert("that_routing_number".into(), Ok(BankBalance::default()));
        let dispatcher = MockTrustBankDispatcher {
            mock_all_reserve_balances: Mock::new(balances),
            ..Default::default()
        };

        let checker = CheckServices::new(poller, &dispatcher);

        // When
        let events: Vec<Vec<(String, String)>> = tracing_capture_event_fields!({
            checker.check_services().unwrap();
        });

        // And we flatten events, and keep only the event strings themselves
        let events: Vec<String> = events
            .into_iter()
            .flatten()
            .map(|(_key, event_str)| event_str)
            .collect();

        // Then
        let expected_xand_api_health_check_event =
            LoggingEvent::XandApiHealthCheckPassed.debug_fmt();
        let expected_balance_event_1 = LoggingEvent::BankHealthCheckPassed {
            routing_number: "this_routing_number".into(),
        }
        .debug_fmt();
        let expected_balance_event_2 = LoggingEvent::BankHealthCheckPassed {
            routing_number: "that_routing_number".into(),
        }
        .debug_fmt();

        assert!(events.contains(&expected_xand_api_health_check_event));
        assert!(events.contains(&expected_balance_event_1));
        assert!(events.contains(&expected_balance_event_2))
    }

    #[test]
    fn health_check__emits_error_event_if_unhealthy() {
        // Given
        let fake_xand_api_error = XandApiClientError::BadRequest {
            message: "fake xand-api client error".into(),
        };
        let erroring_client = ConfigurableMockXandApiClient {
            get_total_issuance: Mock::new(Err(fake_xand_api_error.clone())),
            ..Default::default()
        };
        let mock_client = Box::new(erroring_client);

        let create_processor = FakeCreateProcessor::default();
        let redeem_processor = FakeRedeemProcessor::default();
        let poller = Poller::new_with_client(
            Arc::new(Runtime::new().unwrap()),
            mock_client,
            create_processor,
            redeem_processor,
        );
        let mut balances = HashMap::new();
        balances.insert("routing_number".into(), Err(BankError::UnknownError));
        let dispatcher = MockTrustBankDispatcher {
            mock_all_reserve_balances: Mock::new(balances),
            ..Default::default()
        };
        let checker = CheckServices::new(poller, &dispatcher);

        // When
        let events: Vec<Vec<(String, String)>> = tracing_capture_event_fields!({
            checker.check_services().unwrap_err();
        });
        // And we flatten events, and keep only the event strings themselves
        let events: Vec<String> = events
            .into_iter()
            .flatten()
            .map(|(_key, event_str)| event_str)
            .collect();

        // Then
        let expected_event_1 = LoggingEvent::XandApiHealthCheckFailed {}.debug_fmt();
        let expected_event_2 = LoggingEvent::BankHealthCheckFailed {
            routing_number: "routing_number".into(),
        }
        .debug_fmt();
        let expected_error_report: HealthCheckErrorReport = vec![
            TrustError::XandApiClient {
                msg: "Error while fetching total claims during health check".to_string(),
                source: fake_xand_api_error,
            },
            BankError::UnknownError.into(),
        ]
        .into();
        assert!(events.contains(&expected_event_1));
        assert!(events.contains(&expected_event_2));
        assert!(events.contains(&expected_error_report.to_string()));
    }
}
