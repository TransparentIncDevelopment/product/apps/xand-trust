use crate::{
    core::{CreateProcessor, RedeemProcessor, ServicesChecker, TrustBankDispatcher},
    errors::TrustError,
    logging_events::LoggingEvent,
    status::CheckServices,
};
use std::{
    sync::atomic::{self, AtomicBool},
    thread, time,
};

/// The generic types had to be repeated since resolving between
/// service checker and poller was not able to be resolved in a
/// short time frame. There is a TODO here if this can be cleaned
/// up here.
#[derive(Constructor)]
pub struct TrusteeEngine<'a, BD: TrustBankDispatcher, C: CreateProcessor, R: RedeemProcessor> {
    polling_delay: u64,
    status_check_iterations: u64,
    service_checker: CheckServices<'a, BD, C, R>,
}

impl<'a, BD: TrustBankDispatcher, C: CreateProcessor, R: RedeemProcessor>
    TrusteeEngine<'a, BD, C, R>
{
    /// Main running loop for the trustee node. Polls the xand network Api every [polling_delay]
    /// seconds for any outstanding create requests or unfulfilled redeems, and sends them off to their
    /// corresponding handler. Also performs health checks every [status_check_iterations] *
    /// [polling_delay].
    ///
    /// The `stop` flag indicates if the trustee should stop.
    pub fn run(&mut self, stop: &AtomicBool) -> Result<(), TrustError> {
        let mut status_iteration = 0;
        tracing::info!(message = ?LoggingEvent::TrustStarted);
        while !stop.load(atomic::Ordering::SeqCst) {
            if status_iteration == 0 {
                Self::check_health_status(&self.service_checker);
            }
            match self.service_checker.poller().poll_for_new_requests() {
                Ok(_) => {}
                // TODO: Make specific "during polling" error or add message or what have you
                Err(e) => tracing::error!(message = ?e ),
            }
            thread::sleep(time::Duration::new(self.polling_delay, 0));
            status_iteration = (status_iteration + 1) % self.status_check_iterations;
        }
        Ok(())
    }

    #[tracing::instrument(skip(services_checker))]
    fn check_health_status<SC: ServicesChecker>(services_checker: &SC) {
        match services_checker.check_services() {
            Ok(_) => tracing::info!(message = ?LoggingEvent::HealthCheckSuccessful),
            Err(report) => {
                let event = LoggingEvent::HealthCheckFailed { report };
                tracing::error!(message = ?event);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::errors::HealthCheckErrorReport;
    use crate::test_utils::{FakeCreateProcessor, FakeRedeemProcessor, MockTrustBankDispatcher};
    use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
    use tracing_assert_macros::tracing_capture_event_fields;

    struct MockServicesChecker {
        result: Result<(), HealthCheckErrorReport>,
    }
    impl ServicesChecker for MockServicesChecker {
        fn check_services(&self) -> Result<(), HealthCheckErrorReport> {
            self.result.clone()
        }
    }

    #[test]
    fn check_health_status__produces_event_on_failure() {
        // Given
        let health_check_err_report: HealthCheckErrorReport =
            vec![TrustError::FakeTestError {}].into();
        let failing_services_checker = MockServicesChecker {
            result: Err(health_check_err_report.clone()),
        };

        // When
        let events: Vec<Vec<(String, String)>> = tracing_capture_event_fields!({
            TrusteeEngine::<MockTrustBankDispatcher, FakeCreateProcessor, FakeRedeemProcessor>::check_health_status(
                &failing_services_checker,
            )
        });

        // Then
        let expected_event = vec![(
            "message".into(),
            LoggingEvent::HealthCheckFailed {
                report: health_check_err_report,
            }
            .debug_fmt(),
        )];

        assert!(events.contains(&expected_event));
    }

    #[test]
    fn check_health_status__produces_event_on_success() {
        // Given
        let services_checker = MockServicesChecker { result: Ok(()) };

        // When
        let events: Vec<Vec<(String, String)>> = tracing_capture_event_fields!({
            TrusteeEngine::<MockTrustBankDispatcher, FakeCreateProcessor, FakeRedeemProcessor>::check_health_status(
                &services_checker,
            )
        });

        // Then
        let expected_event = vec![(
            "message".into(),
            LoggingEvent::HealthCheckSuccessful.debug_fmt(),
        )];

        assert!(events.contains(&expected_event));
    }
}
