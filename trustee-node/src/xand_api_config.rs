#![allow(non_snake_case)]
use super::Result;
use crate::secret_store::create_secret_store;
use futures::executor::block_on;
use std::borrow::Borrow;
use std::time::Duration;
use trustee_node_config::TrusteeConfig;
use url::Url;
use xand_secrets::ExposeSecret;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct XandApiConfig {
    pub xand_api_endpoint: Url,
    pub xand_api_jwt: Option<String>,
    pub timeout_seconds: Option<Duration>,
}

impl XandApiConfig {
    pub fn from_trust_config(config: &TrusteeConfig) -> Result<Self> {
        let secret_store = create_secret_store(&config.secret_store);
        let secret = match config.xand_api_jwt_secret_key.borrow() {
            Some(key) => {
                let result = block_on(secret_store.read(key))?;
                Some(result.expose_secret().to_string())
            }
            None => None,
        };

        Ok(XandApiConfig {
            xand_api_endpoint: config.xand_api_endpoint.clone(),
            xand_api_jwt: secret,
            timeout_seconds: config.xand_api_timeout_seconds,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::errors::TrustError;
    use std::{collections::BTreeMap, io::Write};
    use tempfile::NamedTempFile;
    use trustee_node_config::{LocalFileSecretStoreConfiguration, SecretStoreConfig};

    fn create_secret_store_file(key_value_secrets: Vec<(&str, &str)>) -> NamedTempFile {
        let key_value_map: BTreeMap<String, String> = key_value_secrets
            .into_iter()
            .map(|(key, value)| (key.to_string(), value.to_string()))
            .collect();
        // Serialize it to a YAML string.
        let secret_store_content = serde_yaml::to_string(&key_value_map).unwrap();

        //write yaml to file
        let mut file = NamedTempFile::new().unwrap();
        file.write_all(secret_store_content.as_bytes()).unwrap();
        file
    }

    fn make_trustee_config(
        xand_api_url: &Url,
        xand_api_jwt_key: Option<&str>,
        xand_api_timeout_seconds: Option<Duration>,
        secret_store_path: String,
    ) -> TrusteeConfig {
        TrusteeConfig {
            xand_api_endpoint: xand_api_url.clone(),
            xand_api_jwt_secret_key: xand_api_jwt_key.map(|key| key.to_string()),
            xand_api_timeout_seconds,
            secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
                yaml_file_path: secret_store_path,
            }),
            ..Default::default()
        }
    }

    #[test]
    fn from_trust_config__creates_jwt_value_from_key_and_secret_store() {
        // given
        let token = "super_secret_token";
        let xand_url = Url::parse("http://some-url/").unwrap();
        let secret_store_file = create_secret_store_file(vec![("the_jwt_key", token)]);
        let secret_store_path = secret_store_file.path().to_str().unwrap().to_string();
        let trust_config =
            make_trustee_config(&xand_url, Some("the_jwt_key"), None, secret_store_path);

        // when
        let xand_config = XandApiConfig::from_trust_config(&trust_config);
        secret_store_file.close().unwrap();

        // then
        let expected_xand_config = XandApiConfig {
            xand_api_endpoint: xand_url,
            xand_api_jwt: Some(token.to_string()),
            timeout_seconds: None,
        };
        assert_eq!(xand_config.unwrap(), expected_xand_config);
    }

    #[test]
    fn from_trust_config__returns_error_when_jwt_key_not_found() {
        // given
        let xand_url = Url::parse("http://some-url/").unwrap();
        let secret_store_file = create_secret_store_file(vec![]);
        let secret_store_path = secret_store_file.path().to_str().unwrap().to_string();
        let trust_config =
            make_trustee_config(&xand_url, Some("the_jwt_key"), None, secret_store_path);

        // when
        let xand_config = XandApiConfig::from_trust_config(&trust_config);
        secret_store_file.close().unwrap();

        // then
        assert!(matches!(
            xand_config,
            Err(TrustError::ReadSecretError { .. })
        ));
    }

    #[test]
    fn from_trust_config__returns_empty_jwt_when_key_is_none() {
        // given
        let token = None;
        let xand_url = Url::parse("http://some-url/").unwrap();
        let secret_store_file = create_secret_store_file(vec![]);
        let secret_store_path = secret_store_file.path().to_str().unwrap().to_string();
        let trust_config = make_trustee_config(&xand_url, token, None, secret_store_path);

        // when
        let xand_config = XandApiConfig::from_trust_config(&trust_config);
        secret_store_file.close().unwrap();

        // then
        let expected_xand_config = XandApiConfig {
            xand_api_endpoint: xand_url,
            xand_api_jwt: None,
            timeout_seconds: None,
        };
        assert_eq!(xand_config.unwrap(), expected_xand_config);
    }

    #[test]
    fn from_trust_config__passes_correct_xand_api_timeout() {
        // given
        let token = "super_secret_token";
        let xand_url = Url::parse("http://some-url/").unwrap();
        let secret_store_file = create_secret_store_file(vec![("the_jwt_key", token)]);
        let secret_store_path = secret_store_file.path().to_str().unwrap().to_string();
        let expected_xand_api_timeout_seconds = Some(Duration::from_secs(60));
        let trust_config = make_trustee_config(
            &xand_url,
            Some("the_jwt_key"),
            expected_xand_api_timeout_seconds,
            secret_store_path,
        );

        // when
        let xand_config = XandApiConfig::from_trust_config(&trust_config);
        secret_store_file.close().unwrap();

        // then
        assert_eq!(
            xand_config.unwrap().timeout_seconds,
            expected_xand_api_timeout_seconds
        );
    }
}
