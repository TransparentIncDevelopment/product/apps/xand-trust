use crate::{core::BankError, LoggingEvent};
use bank_transaction_repository::{
    error::Error as BtrError, BankAccount, BankTransactionRepository, HistoryRequest,
    SqliteDatabaseClient, Transaction, TransactionDirection,
};
use std::sync::Arc;
use tokio::runtime::Runtime;
use trustee_node_config::Account;
use xand_banks::{
    date_range::DateRange,
    models::{BankTransaction, BankTransactionType},
};

type Result<T, E = HistoryFetchError> = std::result::Result<T, E>;

pub trait HistoryFetcher {
    fn get_history(&self, account: Account, range: DateRange) -> Result<Vec<BankTransaction>>;
}

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Clone, Snafu)]
#[snafu(visibility(pub(crate)))]
pub enum HistoryFetchError {
    #[snafu(display("Initialization error: {}", source))]
    InitializationError { source: Arc<BtrError> },
    #[snafu(display("Periodic synchronization error: {}", source))]
    PeriodicSynchronizationError { source: Arc<BtrError> },
    #[snafu(display("Query error: {}", source))]
    HistoryError { source: Arc<BtrError> },
}

impl From<HistoryFetchError> for BankError {
    fn from(source: HistoryFetchError) -> Self {
        BankError::FetchError { source }
    }
}

impl From<BtrError> for HistoryFetchError {
    fn from(e: BtrError) -> Self {
        match e {
            BtrError::SqliteDatabaseInitializationError(_)
            | BtrError::SecretStoreCreationError(_)
            | BtrError::HistoryProviderCreationError(_) => HistoryFetchError::InitializationError {
                source: Arc::new(e),
            },
            BtrError::PeriodicError(_) => HistoryFetchError::PeriodicSynchronizationError {
                source: Arc::new(e),
            },
            BtrError::HistoryPullerError(_) => HistoryFetchError::HistoryError {
                source: Arc::new(e),
            },
        }
    }
}

pub trait HistoryAdapter {
    fn get_history(&self, request: &HistoryRequest) -> Result<Vec<Transaction>, BtrError>;
}

pub struct BtrHistoryAdapter {
    btr: BankTransactionRepository<Arc<SqliteDatabaseClient>, Arc<SqliteDatabaseClient>>,
    rt: Arc<Runtime>,
}

impl BtrHistoryAdapter {
    fn new(
        rt: Arc<Runtime>,
        btr: BankTransactionRepository<Arc<SqliteDatabaseClient>, Arc<SqliteDatabaseClient>>,
    ) -> Self {
        Self { rt, btr }
    }
}

impl HistoryAdapter for BtrHistoryAdapter {
    fn get_history(&self, request: &HistoryRequest) -> Result<Vec<Transaction>, BtrError> {
        self.rt.block_on(self.btr.get_history(request))
    }
}

pub struct BtrHistoryFetcher<Adapter: HistoryAdapter> {
    adapter: Adapter,
}

impl BtrHistoryFetcher<BtrHistoryAdapter> {
    pub fn new(
        rt: Arc<Runtime>,
        btr: BankTransactionRepository<Arc<SqliteDatabaseClient>, Arc<SqliteDatabaseClient>>,
    ) -> Self {
        Self {
            adapter: BtrHistoryAdapter::new(rt, btr),
        }
    }
}

impl<Adapter: HistoryAdapter> BtrHistoryFetcher<Adapter> {
    fn bank_transaction_from_btr_transaction(txn: &Transaction) -> BankTransaction {
        let txn_type = match txn.get_credit_or_debit() {
            TransactionDirection::Credit => BankTransactionType::Credit,
            TransactionDirection::Debit => BankTransactionType::Debit,
        };
        BankTransaction::new(txn.get_id(), txn.get_amount(), txn.get_metadata(), txn_type)
    }
}

impl<Adapter: HistoryAdapter> HistoryFetcher for BtrHistoryFetcher<Adapter> {
    #[tracing::instrument(skip(self, account))]
    fn get_history(
        &self,
        account: trustee_node_config::Account,
        date_range: xand_banks::date_range::DateRange,
    ) -> Result<Vec<BankTransaction>> {
        tracing::info!(message = ?LoggingEvent::QueryingBankTransactionRepository);
        let bank_account = BankAccount::new(account.routing_number, account.account_number);
        let request = HistoryRequest::new(bank_account, date_range.begin_date());
        let transactions = self
            .adapter
            .get_history(&request)?
            .iter()
            .map(|txn| Self::bank_transaction_from_btr_transaction(txn))
            .collect();
        tracing::debug!(message = ?LoggingEvent::BankTransactionRepositorySuccessfullyQueried);
        Ok(transactions)
    }
}

#[cfg(test)]
mod btr_history_fetcher_tests {
    #![allow(non_snake_case)]

    use super::{BtrHistoryFetcher, HistoryAdapter, HistoryFetchError, HistoryFetcher};
    use crate::LoggingEvent;
    use assert_matches::assert_matches;
    use bank_transaction_repository::{
        error::Error as BtrError, BankAccount, HistoryRequest, Transaction, TransactionDirection,
    };
    use snafu::Snafu;
    use std::sync::{Arc, Mutex};
    use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
    use tracing_assert_macros::tracing_capture_event_fields;
    use trustee_node_config::Account;
    use xand_banks::{
        date_range::DateRange,
        xand_money::{Money, Usd},
    };

    #[derive(Debug, Snafu)]
    pub enum TestError {
        #[snafu(display("Test Error"))]
        Test,
    }

    #[derive(Debug, Clone)]
    struct MockHistoryAdapter {
        requests: Arc<Mutex<Vec<HistoryRequest>>>,
        response: Arc<Mutex<Result<Vec<Transaction>, BtrError>>>,
    }

    impl MockHistoryAdapter {
        fn new(response: Result<Vec<Transaction>, BtrError>) -> Self {
            Self {
                requests: Default::default(),
                response: Arc::new(Mutex::new(response)),
            }
        }
        fn get_requests(&self) -> Vec<HistoryRequest> {
            self.requests.lock().unwrap().clone()
        }
    }

    impl HistoryAdapter for MockHistoryAdapter {
        fn get_history(
            &self,
            request: &bank_transaction_repository::HistoryRequest,
        ) -> super::Result<Vec<Transaction>, BtrError> {
            self.requests.lock().unwrap().push(request.clone());

            match self.response.lock().unwrap().as_ref() {
                Ok(txns) => Ok(txns.clone()),
                Err(_) => Err(BtrError::SecretStoreCreationError(Box::new(
                    TestError::Test,
                ))),
            }
        }
    }

    #[test]
    fn get_history__returns_proper_error() {
        // Given a failing repository
        let expected_error = Err(BtrError::SecretStoreCreationError(Box::new(
            TestError::Test,
        )));
        let adapter = MockHistoryAdapter::new(expected_error);
        let subject = BtrHistoryFetcher::<MockHistoryAdapter> { adapter };

        // When get_history is called
        let error = subject
            .get_history(
                Account::new("account number".into(), "routing number".into()),
                DateRange::default(),
            )
            .unwrap_err();

        // Then the returned error should match the expected error
        assert_matches!(error, HistoryFetchError::InitializationError { .. });
    }

    #[test]
    fn get_history__returns_expected_history() {
        // Given a repository populated with history
        let expected_history = vec![Transaction::new(
            Usd::from_i64_minor_units(100).unwrap(),
            TransactionDirection::Credit,
            "metadata".to_string(),
        )];
        let adapter = MockHistoryAdapter::new(Ok(expected_history.clone()));
        let subject = BtrHistoryFetcher::<MockHistoryAdapter> { adapter };

        // When get_history is called
        let result = subject
            .get_history(
                Account::new("account number".into(), "routing number".into()),
                DateRange::default(),
            )
            .unwrap();

        // Then the returned history should match the expected history
        assert_eq!(
            result,
            expected_history
                .iter()
                .map(|txn| {
                    BtrHistoryFetcher::<MockHistoryAdapter>::bank_transaction_from_btr_transaction(
                        txn,
                    )
                })
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn get_history__adapter_is_passed_correct_args() {
        // Given
        let account = BankAccount::new(
            String::from("routing number"),
            String::from("account number"),
        );
        let date_range = DateRange::default();
        let expected_request = HistoryRequest::new(account.clone(), date_range.begin_date());

        let adapter = MockHistoryAdapter::new(Ok(Default::default()));
        let subject = BtrHistoryFetcher::<MockHistoryAdapter> {
            adapter: adapter.clone(),
        };

        // When get_history is called
        let _result = subject
            .get_history(
                Account::new(account.get_account_number(), account.get_routing_number()),
                date_range,
            )
            .unwrap();

        // Then it should have been passed the correct arguments
        assert_eq!(adapter.get_requests(), vec![expected_request]);
    }

    #[test]
    fn get_history__traces_btr_interaction_begin_and_end() {
        // Given a repository populated with history
        let expected_history = vec![Transaction::new(
            Usd::from_i64_minor_units(100).unwrap(),
            TransactionDirection::Credit,
            "metadata".to_string(),
        )];
        let adapter = MockHistoryAdapter::new(Ok(expected_history));
        let subject = BtrHistoryFetcher::<MockHistoryAdapter> { adapter };

        // When get_history is called
        let events = tracing_capture_event_fields!({
            subject
                .get_history(
                    Account::new("account number".into(), "routing number".into()),
                    DateRange::default(),
                )
                .unwrap();
        });

        let expected_event_begin: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::QueryingBankTransactionRepository.debug_fmt(),
        )];
        let expected_event_end: Vec<(String, String)> = vec![(
            "message".to_string(),
            LoggingEvent::BankTransactionRepositorySuccessfullyQueried.debug_fmt(),
        )];
        assert!(events.contains(&expected_event_begin));
        assert!(events.contains(&expected_event_end));
    }
}
