use std::time::Instant;
use thiserror::Error;
use uuid::Uuid;
use xand_api_client::{CorrelationId, PendingCreateRequest, PendingRedeemRequest};

#[derive(Clone, Debug, Error, Serialize)]
pub enum Error {
    // TODO: See ADO 8019-8022 - this placeholder can be removed and actual errors populated as the Create/Redeem Collectors are implemented
    #[error("StatusStore: {0}")]
    StatusStore(String),
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

pub enum RedeemStatus {
    Started(Claimer),
    AwaitingCompletion(Claimer),
    AwaitingCancellation(Claimer),
    BankTransferStateUnknown,
    AwaitingBankTransfer(Claimer),
    Unclaimed,
}

pub struct RedeemRequest {
    status: RedeemStatus,
    timestamp: Instant,
    tracking_no: Uuid,
    pending_redeem: PendingRedeemRequest,
}

impl RedeemRequest {
    pub fn status(&self) -> &RedeemStatus {
        &self.status
    }
    pub fn timestamp(&self) -> &Instant {
        &self.timestamp
    }
    pub fn tracking_no(&self) -> &Uuid {
        &self.tracking_no
    }
    pub fn pending_redeem(&self) -> &PendingRedeemRequest {
        &self.pending_redeem
    }
}

pub trait RedeemStatusStore {
    fn get_unclaimed_redeems(&self) -> Result<Vec<RedeemRequest>>;
    fn add_redeem(&self, redeem: RedeemRequest) -> Result<()>;
    fn update_redeem(&self, redeem: RedeemRequest) -> Result<()>;
    fn get_redeems_by_status(&self, statuses: &[RedeemStatus]) -> Result<Vec<RedeemRequest>>;
    fn get_redeems_by_id(&self, statuses: &[CorrelationId]) -> Result<Vec<RedeemRequest>>;
}

pub trait RedeemCollector {
    fn process(request: RedeemRequest) -> Result<()>;
}

pub enum CreateStatus {
    Started(Claimer),
    AwaitingConfirmation(Claimer),
    AwaitingCancellation(Claimer),
    Unclaimed,
}

pub struct CreateRequest {
    status: CreateStatus,
    timestamp: Instant,
    tracking_no: Uuid,
    pending_create: PendingCreateRequest,
}

impl CreateRequest {
    pub fn status(&self) -> &CreateStatus {
        &self.status
    }
    pub fn timestamp(&self) -> &Instant {
        &self.timestamp
    }
    pub fn tracking_no(&self) -> &Uuid {
        &self.tracking_no
    }
    pub fn pending_create(&self) -> &PendingCreateRequest {
        &self.pending_create
    }
}

pub trait CreateStatusStore {
    fn get_unclaimed_creates(&self) -> Result<Vec<CreateRequest>>;
    fn add_create(&self, create: CreateRequest) -> Result<()>;
    fn update_create(&self, redeem: CreateRequest) -> Result<()>;
    fn get_creates_by_status(&self, statuses: &[RedeemStatus]) -> Result<Vec<CreateRequest>>;
    fn get_creates_by_id(&self, id: &[CorrelationId]) -> Result<Vec<CreateRequest>>;
}

pub trait CreateCollector {
    fn process(request: CreateRequest) -> Result<()>;
}

#[derive(Default)]
pub struct Claimer {
    id: Uuid,
}

//#[allow(clippy::new_without_default)]
impl Claimer {
    pub fn new() -> Self {
        let id = Uuid::new_v4();
        Claimer { id }
    }
    pub fn id(&self) -> &Uuid {
        &self.id
    }
}

pub struct PendingCreate {}

pub struct PendingRedeem {}
