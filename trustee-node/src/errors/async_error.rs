#[derive(Debug, Snafu, Clone, Serialize)]
#[snafu(visibility(pub(crate)))]
pub enum AsyncError {
    #[snafu(display("{}", message))]
    TokioRuntimeInitializiation { message: String },
}
