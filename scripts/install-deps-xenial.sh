#!/usr/bin/env bash

set -o errexit
set -o nounset

if [ $EUID -ne 0 ]; then
    echo "Please run as root." 1>&2
    exit 1
fi

function no_exe() {
    ! command -v $1 >/dev/null 2>&1
}

function wrong_protoc_version() {
    test $(protoc --version | cut -d' ' -f2 | sed 's/\([0-9]\).*/\1/') -ne 3
}

export CARGO_HOME=${CARGO_HOME:-$HOME/.cargo}

apt-get update -yqq

apt-get install -yqq --no-install-recommends \
    build-essential \
    curl \
    pkg-config \
    openssl \
    unzip \
    ca-certificates

if no_exe rustup; then
    curl https://sh.rustup.rs -sSf > /usr/bin/rustup-init
    chmod +x /usr/bin/rustup-init
    rustup-init -y
    source $CARGO_HOME/env
fi

if no_exe protoc || wrong_protoc_version; then
    PROTOC_DIR="/opt/protoc3"
    PROTOC_ZIP="protoc-3.5.1-linux-x86_64.zip"
    curl -OLsS "https://github.com/google/protobuf/releases/download/v3.5.1/${PROTOC_ZIP}"
    unzip $PROTOC_ZIP -d $PROTOC_DIR
    rm $PROTOC_ZIP
fi

cargo install cargo-deb || true
